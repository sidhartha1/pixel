@if(Session::has('c_id'))
<!-- Customer Id Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', Session::get('c_id'), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:none">
        {!! Form::label('customer_name', 'Customer Name:') !!}
        {!! Form::text('customer_name', Session::get('c_name'), ['class' => 'form-control','readonly']) !!}
</div>
@endif

<!-- Product Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_name', 'Product Name:') !!}
    {!! Form::text('product_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Category Field -->
<div class="form-group col-sm-6"  style="display:none">
    {!! Form::label('product_category', 'Product Category:') !!}
    {!! Form::select('product_category', ['Mobiles' => 'Mobiles',
										  'Electronics' => 'Electronics',
										  'Electronic Accessories'=>'Electronic Accessories',
										  'Home Appliances'=>'Home Appliances',
										  'Groceries'=>'Groceries',
										  'Footwear'=>'Footwear',
										  'Clothing Accessories'=>'Clothing Accessories',
										  'Fashion'=>'Fashion',
										  'Sports, Fitness & Outdoors'=>'Sports, Fitness & Outdoors',
										  'Toys & Baby Products'=>'Toys & Baby Products',
										  'Beauty & Health'=>'Beauty & Health',
										  'Home & Kitchen'=>'Home & Kitchen',
										  'Other'=>'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::select('quantity', ['1' => '1', '2' => '2', '3'=>'3', '4'=>'4', '5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_cost', 'Shipping Cost:') !!}
    {!! Form::number('shipping_cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field
<div class="form-group col-sm-6">
    {!! Form::label('total_cost', 'Total Cost:') !!}
    {!! Form::number('total_cost', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paid', 'Paid:') !!}
    {!! Form::number('paid', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::datetime('date', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d H:m:s'), ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(user()->role=="storeowner")
        <a href="{!! route('home') !!}" class="btn btn-default"> Cancel</a>
    @else
        <a href="{!! route('wydr.sales') !!}" class="btn btn-default">Cancel</a>
    @endif
</div>
