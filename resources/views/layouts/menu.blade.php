<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{!! route('home') !!}"><i class="fa fa-pencil-square-o"></i><span>Sales Entry</span></a>
</li>

<li class="{{ Request::is('storeOnboarding*') ? 'active' : '' }}" style="display:none">
    <a href="{!! route('storeOnboarding.index') !!}"><i class="fa fa-sign-in"></i><span>Onboarding</span></a>
</li>

@if(Auth::user()->role=='admin')

<li class="{{ Request::is('storeOwners*') ? 'active' : '' }}">
    <a href="{!! route('storeOwners.index') !!}"><i class="fa fa-edit"></i><span>StoreOwners</span></a>
</li>

<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-users"></i><span>Customers</span></a>
</li>

<li class="{{ Request::is('onboardings*') ? 'active' : '' }}">
    <a href="{!! route('onboardings.index') !!}"><i class="fa fa-edit"></i><span>Onboardings</span></a>
</li>
<li class="{{ Request::is('userResponses*') ? 'active' : '' }}">
    <a href="{!! route('userResponses.index') !!}"><i class="fa fa-edit"></i><span>UserResponses</span></a>
</li>
<li class="{{ Request::is('medlife*') ? 'active' : '' }}">
    <a href="{!! route('home') !!}" onclick="alert('Coming Soon!');"><i class="fa fa-medkit"></i><span>Medicines</span></a>
</li>
@else

<li class="{{ Request::is('loanyantra*') ? 'active' : '' }}">
    <a href="{!! route('loanyantra.index') !!}"><i class="fa fa-money"></i><span>Loans</span></a>
</li>

<li class="{{ Request::is('reports*') ? 'active' : '' }}">
    <a href="{!! route('reports.index') !!}"><i class="fa fa-rupee"></i><span>Earnings</span></a>
</li>

<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-users"></i><span>Customers</span></a>
</li>

<li class="{{ Request::is('storeSales*') ? 'active' : '' }}">
    <a href="{!! route('storeSales') !!}"><i class="fa fa-shopping-bag"></i><span>Store Sales</span></a>
</li>

<!-- <li class="{{ Request::is('sales*') ? 'active' : '' }}"> -->
<!--     <a href="{!! route('sales.index') !!}"><i class="fa fa-amazon"></i><span>Amazon Sales</span></a> -->
<!-- </li> -->
@endif


<li class="{{ Request::is('support*') ? 'active' : '' }}">
    <a href="{!! route('support') !!}"><i class="fa fa-headphones"></i><span>Support</span></a>
</li>


<!-- <li class="{{ Request::is('onemg*') ? 'active' : '' }}"> -->
<!--     <a href="{!! route('onemg.index') !!}"><i class="fa fa-medkit"></i><span>Medicines Sales</span></a> -->
<!-- </li> -->

<!-- <li class="{{ Request::is('wydr*') ? 'active' : '' }}"> -->
<!--     <a href="{!! route('wydr.index') !!}"><i class="fa fa-wikipedia-w"></i><span>Wydr Sales</span></a> -->
<!-- </li> -->

<li class="{{ Request::is('ibpay*') ? 'active' : '' }}">
    <a href="{!! route('ibpay') !!}"><i class="fa fa-credit-card"></i><span>ibPay</span></a>
</li>

<li class="{{ Request::is('zestview*') ? 'active' : '' }}">
    <a href="{!! route('zestview') !!}"><i class="fa fa-shopping-bag"></i><span>Zest || EMI</span></a>
</li>
<!--
<li class="{{ Request::is('insurance*') ? 'active' : '' }}">
    <a href="{!! route('insurance') !!}"><i class="fa fa-shopping-bag"></i><span>Insurance</span></a>
</li> -->


<li class="{{ Request::is('knowledge*') ? 'active' : '' }}">
    <a href="{!! route('knowledge') !!}"><i class="fa fa-book"></i><span>Knowledge Bank</span></a>
</li>

<!-- <li class="{{ Request::is('orders*') ? 'active' : '' }}"> -->
<!--     <a href="{!! route('orders.index') !!}"><i class="fa fa-amazon"></i><i class="fa fa-check"></i><span>Amazon Tracked Orders</span></a> -->
<!-- </li> -->

<!--<li class="{{ Request::is('reports*') ? 'active' : '' }}">
    <a href="{!! route('reports.index') !!}"><i class="fa fa-inr"></i><span>Earnings</span></a>
</li>-->
