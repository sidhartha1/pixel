<table class="table table-responsive" id="sales-table">
    <thead>
        <th>Customer Name</th>
        <th>Product Name</th>
        <th>Order Id</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
        <th>Date & Time</th>

    </thead>

    <tbody>
    @foreach($zestmoney_sales as $sale)
        <tr>
            <td>{!! $sale->customer_name !!}</td>
            <td>{!! $sale->product_name !!}</td>
            <td>{!! $sale->order_id !!}</td>
            <td>{!! $sale->quantity !!}</td>
            <td>{!! $sale->price !!}</td>
            <td>{!! $sale->total_cost !!}</td>
            <td>{!! $sale->created_at->setTimeZone(new DateTimeZone("Asia/Kolkata")) !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>