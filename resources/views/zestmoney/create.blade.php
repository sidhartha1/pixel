@extends('layouts.app')

@section('content')
    @if(Session::has('customer'))
         $customer=Session::get('customer');
    @endif
    <section class="content-header">
        <h1>
            ZestMoney Sales Entry
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'zestmoney.store']) !!}
                    @include('zestmoney.fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
