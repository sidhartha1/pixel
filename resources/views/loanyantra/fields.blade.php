<div class="form-group col-sm-6">
    {!! Form::label('loan_type', 'Type of Loan:') !!}
    {!! Form::text('loan_type', $loan_type, ['class' => 'form-control', 'size' => '10', 'readonly']) !!}
</div>
<div class="col-sm-12"></div>

<div class="col-sm-12">
    <h4><b>Customer Details</b></h4>
</div>

@if(Session::has('c_name'))
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', Session::get('c_name'), ['class' => 'form-control']) !!}
</div>
@else
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
@endif

@if(Session::has('c_phone'))
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('phone', 'Mobile:') !!}
        {!! Form::text('phone', Session::get('c_phone'), ['class' => 'form-control']) !!}
</div>
@else
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('phone', 'Mobile:') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
@endif

@if(Session::has('c_email'))
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', Session::get('c_email'), ['class' => 'form-control']) !!}
</div>
@else
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
@endif

@if(Session::has('c_age'))
<div class="form-group col-sm-6" style="display:none">
        {!! Form::label('age', 'Age:') !!}<br>
        {!! Form::text('age', Session::get('c_age'), ['class' => 'form-control', 'readonly']) !!}
</div>
@else
<div class="form-group col-sm-6" style="display:none">
        {!! Form::label('age', 'Age:') !!}<br>
        {!! Form::selectRange('age', 22, 60, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Gender Field -->
@if(Session::has('c_gender'))
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::text('gender', Session::get('c_gender'), ['class' => 'form-control', 'readonly']) !!}
</div>
@else
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::radio('gender', 'Male', true) !!} Male
    {!! Form::radio('gender', 'Female', false) !!} Female
</div>
@endif

<!-- Profession Field -->
@if(Session::has('c_profession'))
<div class="form-group col-sm-6"  style="display:none">
    {!! Form::label('profession', 'Profession:') !!}
    {!! Form::text('profession', Session::get('c_profession'), ['class' => 'form-control', 'readonly']) !!}
</div>
@else
<div class="form-group col-sm-6"  style="display:none">
    {!! Form::label('profession', 'Profession:') !!}
    {!! Form::select('profession', ['Student'=>'Student', 'Govt Employee'=>'Govt Employee', 'Private Employee'=>'Private Employee', 'Businessman'=>'Businessman', 'House Wife'=>'House Wife', 'Others'=>'Other'], Session::get('c_profession'), ['class' => 'form-control']) !!}
</div>
@endif

<!-- Source Field -->
@if(Session::has('c_source'))
<div class="form-group col-sm-6"  style="display:none">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::text('source', Session::get('c_source'), ['class' => 'form-control', 'readonly']) !!}
</div>
@else
<div class="form-group col-sm-6"  style="display:none">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::select('source', ['Store Walkin'=>'Store Walkin',
                                'Word of mouth/Referral'=>'Word of mouth/Referral',
                                'Pamphlet'=>'Pamphlet',
                                'WhatsApp'=>'WhatsApp',
                                'SMS Campaign'=>'SMS Campaign',
                                'Billboards'=>'Billboards',
                                'Other'=>'Other'], Session::get('c_source'), ['class' => 'form-control']) !!}
</div>
@endif

<div class="col-sm-12">
    <h4><b>Loan Details</b></h4>
</div>

<div class="form-group col-sm-6">
     <br>
    {!! Form::label('gross_month_salary', 'Gross Monthly Salary (in &#8377;):') !!}
    <input type="text" class="form-control salaryInput" value="25000" onchange="updateSalaryInput(this.value);">
    <input type="range" name="gross_month_salary" class="salaryRange" value="25000" min="0" max="500000" onchange="updateSalaryInput(this.value);">
</div>

<div class="col-sm-12"></div>

<div class="form-group col-sm-6">
     <br>
    {!! Form::label('existing_emis', 'Exisiting EMIs (in &#8377;):') !!}
    <input type="text" class="form-control emiInput" value="0" onchange="updateEmiInput(this.value);">
    <input type="range" name="existing_emis" class="emiRange" value="0" min="0" max="150000" onchange="updateEmiInput(this.value);">
</div>

<div class="col-sm-12"></div>

@if ($loan_type == "New Home Loan" || $loan_type == "Loan Against Property")
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('property_location', 'Property Location:') !!}
        {!! Form::text('property_location', null, ['class' => 'form-control', 'size' => '10']) !!}
</div>
@endif

<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('working_location', 'Working Location:') !!}
        {!! Form::text('working_location', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-12">
    {!! Form::label('job_type', 'Job Type:') !!}
    {!! Form::radio('job_type', 'Self Employed', true, ['onchange' => 'hideCompany()']) !!} Self Employed
    {!! Form::radio('job_type', 'Salaried', false,  ['onchange' => 'showCompany()']) !!} Salaried
</div>

<div class="form-group col-sm-6 company" style="display:none">
        {!! Form::label('company_name', 'Company Name:') !!}
        {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::datetime('date', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d H:m:s'), ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Check Eligibility', ['class' => 'btn btn-primary']) !!}
    @if(user()->role=="storeowner")
        <a href="{!! route('home') !!}" class="btn btn-default"> Cancel</a>
    @else
        <a href="{!! route('loanyantra.index') !!}" class="btn btn-default">Cancel</a>
    @endif
</div>
