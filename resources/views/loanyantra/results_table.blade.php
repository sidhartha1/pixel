<table class="table table-responsive" id="sales-table" style="font-size:14px;width: 100%;">
    <col width="5%">
    <col width="15%">
    <col width="15%">
    <col width="15%">
    <col width="10%">
    <col width="5%">
    <col width="5%">
    <col width="10%">
    <col width="10%">
    <col width="10%">
    
    <thead>
        <th></th>
        <th>Bank Name</th>
        <th>Loan Name</th>
        <th>Description</th>
        <th>Loan Amount</th>
        <th>P_ID</th>
        <th>ROI</th>
        <th>EMI</th>
        <th>Tenure (yrs)</th>
        <th>Processing Fee</th>

    </thead>

    <tbody>
    @foreach($results as $result)
        <tr>
            <td><img style="width:100px;" src="{!! url('http://loanyantra.com/uploadimages/' . $result['BankLogo']) !!}"></td>
            <td>{!! $result['BankName'] !!}</td>
            <td>{!! $result['ProductName'] !!}</td>
            <td>{!! $result['Description'] !!}</td>
            <td>&#8377; {!! $result['LoanAmount'] !!} L</td>
            <td>{!! $result['P_ID'] !!}</td>
            <td>{!! $result['ROI'] !!}</td>
            <td>{!! $result['EMI'] !!}</td>
            <td>{!! $result['Tenure'] !!}</td>
            <td>{!! $result['Processingfee'] !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>
