@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Check Loan Eligibility
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-12">
                   <div class="row">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-block btn-primary" style="height:50px;" onclick="toggle_visibility('loan1')">
                            <p style="font-size:14px;white-space:normal;text-align:center;margin:0 0;">New Home Loan</p>
                            </button>
                            <div class="clearfix"> <br> </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-block btn-primary" style="height:50px;" onclick="toggle_visibility('loan2')">
                            <p style="font-size:14px;white-space:normal;text-align:center;margin:0 0;">Personal Loan</p>
                            </button>
                            <div class="clearfix"> <br> </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-block btn-primary" style="height:50px;" onclick="toggle_visibility('loan3')">
                            <p style="font-size:14px;white-space:normal;text-align:center;margin:0 0;">Business Loan</p>
                            </button>
                            <div class="clearfix"> <br> </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-block btn-primary" style="height:50px;" onclick="toggle_visibility('loan4')">
                            <p style="font-size:14px;white-space:normal;text-align:center;margin:0 0;">Car Loan</p>
                            </button>
                            <div class="clearfix"> <br> </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-block btn-primary" style="height:50px;" onclick="toggle_visibility('loan5')">
                            <p style="font-size:14px;white-space:normal;text-align:center;margin:0 0;">Loan Against Property</p>
                            </button>
                            <div class="clearfix"> <br> </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="loan1" style="display: none">
                    {!! Form::open(['route' => 'loanyantra.store']) !!}
                    @include('loanyantra.fields', ["loan_type" => "New Home Loan"])
                    {!! Form::close() !!}
                </div>
                <div class="row" id="loan2" style="display: none">
                    {!! Form::open(['route' => 'loanyantra.store']) !!}
                    @include('loanyantra.fields', ["loan_type" => "Personal Loan"])
                    {!! Form::close() !!}
                </div>
                <div class="row" id="loan3" style="display: none">
                    {!! Form::open(['route' => 'loanyantra.store']) !!}
                    @include('loanyantra.fields', ["loan_type" => "Business Loan"])
                    {!! Form::close() !!}
                </div>
                <div class="row" id="loan4" style="display: none">
                    {!! Form::open(['route' => 'loanyantra.store']) !!}
                    @include('loanyantra.fields', ["loan_type" => "Car Loan"])
                    {!! Form::close() !!}
                </div>
                <div class="row" id="loan5" style="display: none">
                    {!! Form::open(['route' => 'loanyantra.store']) !!}
                    @include('loanyantra.fields', ["loan_type" => "Loan Against Property"])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function showCompany() {
            var elements = document.querySelectorAll('.company');
            elements.forEach(function(element){
                element.style.display = 'block';
            });
        }
        function hideCompany() {
            var elements = document.querySelectorAll('.company');
            elements.forEach(function(element){
                element.style.display = 'none';
            });
        }
        function updateSalaryInput(val) {
            var elements = document.querySelectorAll('.salaryInput');
            elements.forEach(function(element){
                element.value = val;
            });
            var elements = document.querySelectorAll('.salaryRange');
            elements.forEach(function(element){
                element.value = val;
            });
        }
        function updateEmiInput(val) { 
            var elements = document.querySelectorAll('.emiInput');
            elements.forEach(function(element){
                element.value = val;
            });
            var elements = document.querySelectorAll('.emiRange');
            elements.forEach(function(element){
                element.value = val;
            });
        }
        function toggle_visibility(id) {
            var e = document.getElementById(id);
            console.log(e);
            if (id == 'loan1') {
              document.getElementById('loan2').style.display = 'none';
              document.getElementById('loan3').style.display = 'none';
              document.getElementById('loan4').style.display = 'none';
              document.getElementById('loan5').style.display = 'none';
            }else if (id == 'loan2') {
                document.getElementById('loan1').style.display = 'none';
                document.getElementById('loan3').style.display = 'none';
                document.getElementById('loan4').style.display = 'none';
                document.getElementById('loan5').style.display = 'none';
            }else if (id == 'loan3') {
                document.getElementById('loan1').style.display = 'none';
                document.getElementById('loan2').style.display = 'none';
                document.getElementById('loan4').style.display = 'none';
                document.getElementById('loan5').style.display = 'none';
            }else if (id == 'loan4') {
                document.getElementById('loan1').style.display = 'none';
                document.getElementById('loan2').style.display = 'none';
                document.getElementById('loan3').style.display = 'none';
                document.getElementById('loan5').style.display = 'none';
            }else if (id == 'loan5') {
                document.getElementById('loan1').style.display = 'none';
                document.getElementById('loan2').style.display = 'none';
                document.getElementById('loan3').style.display = 'none';
                document.getElementById('loan4').style.display = 'none';
            }
            if (e.style.display == 'block' || e.style.display=='') e.style.display = 'none';
            else e.style.display = 'block';
        }
    </script>
@endsection
