@extends('layouts.app')

@section('content')
    @if(Session::has('customer'))
         $customer=Session::get('customer');
    @endif
    <section class="content-header">
        <h1>
            Check Loan Eligibility
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-12">
                   <div class="row">
                   {!! Form::open(['route' => 'checkCustomerLoans']) !!}

                   <!-- Phone Field -->
                       <div class="form-group col-sm-3">
                           {!! Form::label('phone', 'Enter Customer Phone No:') !!}
                           {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                       </div>
                       <div class="form-group col-sm-12">
                           {!! Form::submit('Apply for a Loan', ['class' => 'btn btn-primary', 'name' => 'submitbutton']) !!}
                           {!! Form::submit('View Previous Loans', ['class' => 'btn btn-primary', 'name' => 'submitbutton']) !!}
                       </div>

                       {!! Form::close() !!}
                   </div>
                </div>
            </div>
        </div>
    </div>

@endsection