<table class="table table-responsive" id="sales-table" style="font-size:14px;width: 100%;">
    <col width="10%">
    <col width="5%">
    <col width="5%">
    <col width="10%">
    <col width="5%">
    <col width="10%">
    <col width="10%">
    <col width="10%">
    <col width="10%">
    <col width="10%">
    <col width="10%">
    <col width="5%">
    
    <thead>
        <th>Name</th>
        <th>Age</th>
        <th>Mobile</th>
        <th>Gender</th>
        <th>Month Salary</th>
        <th>Existing EMIs</th>
        <th>Property Location</th>
        <th>Working Location</th>
        <th>LoanType</th>
        <th>Company</th>
        <th>Created Date</th>
        <th>Eligibility</th>

    </thead>

    <tbody>
    @foreach($loans as $loan)
        <tr>
            <td>{!! $loan->customer_name !!}</td>
            <td>{!! $loan->customer_age !!}</td>
            <td>{!! $loan->customer_mobile !!}</td>
            <td>{!! $loan->customer_gender !!}</td>
            <td>&#8377;{!! $loan->gross_monthly_salary !!}</td>
            <td>&#8377;{!! $loan->exisiting_emis !!}</td>
            <td>{!! $loan->property_location !!}</td>
            <td>{!! $loan->working_location !!}</td>
            <td>{!! $loan->type_of_loan !!}</td>
            <td>{!! $loan->company_name !!}</td>
            <td>{!! $loan->date !!}</td>
            <td>
            @if ($loan->eligibility == 1)
            Yes
            @else
            No
            @endif
            </td>
        </tr>
    @endforeach
    </tbody>

</table>
