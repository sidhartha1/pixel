@if(mode('edit'))
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', $storeOwner->user->name, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'Leave blank for no change']) !!}
</div>

@else
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::text('password', null, ['class' => 'form-control']) !!}
    </div>
    @endif
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

{{--<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>--}}

<!-- Dob Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dob', 'Dob:') !!}
    {!! Form::date('dob', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::textarea('source', null, ['class' => 'form-control']) !!}
</div>

<!-- Amazon Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amazon_store_id', 'Amazon Store Id:') !!}
    {!! Form::text('amazon_store_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Previous Profession Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Previous_profession', 'Previous Profession:') !!}
    {!! Form::text('Previous_profession', null, ['class' => 'form-control']) !!}
</div>

<!-- Pan Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pan_number', 'Pan Number:') !!}
    {!! Form::text('pan_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Aadhar Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aadhar_number', 'Aadhar Number:') !!}
    {!! Form::text('aadhar_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_num', 'Account Num:') !!}
    {!! Form::text('account_num', null, ['class' => 'form-control']) !!}
</div>

{{--<!-- Onboarding Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('onboarding_status', 'Onboarding Status:') !!}
    {!! Form::text('onboarding_status', null, ['class' => 'form-control']) !!}
</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('storeOwners.index') !!}" class="btn btn-default">Cancel</a>
</div>
