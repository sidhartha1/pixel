<table class="table table-responsive" id="storeOwners-table">
    <thead>
    <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Dob</th>
        <th>Gender</th>
        <th>Phone</th>
        <th>Source</th>
        <th>Amazon Store Id</th>
        <th>Previous Profession</th>
        <th>Pan Number</th>
        <th>Aadhar Number</th>
        <th>Account Num</th>
        <th>Onboarding Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($storeOwners as $storeOwner)
        <tr>
            <td>{!! $storeOwner->user->id !!}</td>
            <td>{!! $storeOwner->user->name !!}</td>
            <td>{!! $storeOwner->email !!}</td>
            <td>{!! $storeOwner->dob !!}</td>
            <td>{!! $storeOwner->gender !!}</td>
            <td>{!! $storeOwner->phone !!}</td>
            <td>{!! $storeOwner->source !!}</td>
            <td>{!! $storeOwner->amazon_store_id !!}</td>
            <td>{!! $storeOwner->Previous_profession !!}</td>
            <td>{!! $storeOwner->pan_number !!}</td>
            <td>{!! $storeOwner->aadhar_number !!}</td>
            <td>{!! $storeOwner->account_num !!}</td>
            <td>{!! $storeOwner->onboarding_status !!}</td>
            <td>
                {!! Form::open(['route' => ['storeOwners.destroy', $storeOwner->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('storeOwners.show', [$storeOwner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('storeOwners.edit', [$storeOwner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $storeOwners->links() !!}
