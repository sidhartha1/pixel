    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
        <div class="row">
            {!! Form::open(['route'=>'check']) !!}
            {{--<iframe width="854" height="480" src="https://www.youtube.com/embed/_3FiPmgqZd0" frameborder="0" allowfullscreen></iframe>--}}
            {{--{!! dd($videos) !!}--}}
            {{--@if($videos->first()==$video)--}}
                <iframe width="854" height="480" src="https://www.youtube.com/embed/{{$videos->first()->url}}" frameborder="0" allowfullscreen>
                </iframe>
            <input type="hidden" name="video_url" value="{{$videos->first()->url}}">
            {{--{!! Form::text('video_url',$videos->first()->url) !!}--}}
            {{--@endif--}}


            @foreach($videos as $video)

            <div class="form-group">
                {!!$video->question !!}
            </div>

                <div class="form-group">
                    {!!Form::radio('option.'.$video->id, $video->option_1); !!}
                    {!! Form::label('option.'.$video->id, $video->option_1) !!}
                </div>
                <div class="form-group">
                    {!!Form::radio('option.'.$video->id, $video->option_2); !!}
                    {!! Form::label('option.'.$video->id, $video->option_2) !!}
                </div>
                <div class="form-group">
                    {!!Form::radio('option.'.$video->id, $video->option_3); !!}
                    {!! Form::label('option.'.$video->id, $video->option_3) !!}
                </div>
                <div class="form-group">
                    {!!Form::radio('option.'.$video->id, $video->option_4); !!}
                    {!! Form::label('option.'.$video->id, $video->option_4) !!}
                </div>



                @endforeach
            <div class="form-group col-sm-12">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
            </div>
        </div>
    </div>
