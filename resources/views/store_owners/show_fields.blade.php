<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $storeOwner->user->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $storeOwner->user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $storeOwner->email !!}</p>
</div>

{{--<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $storeOwner->password !!}</p>
</div>--}}

<!-- Dob Field -->
<div class="form-group">
    {!! Form::label('dob', 'Dob:') !!}
    <p>{!! $storeOwner->dob !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $storeOwner->gender !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $storeOwner->phone !!}</p>
</div>

<!-- Source Field -->
<div class="form-group">
    {!! Form::label('source', 'Source:') !!}
    <p>{!! $storeOwner->source !!}</p>
</div>

<!-- Amazon Store Id Field -->
<div class="form-group">
    {!! Form::label('amazon_store_id', 'Amazon Store Id:') !!}
    <p>{!! $storeOwner->amazon_store_id !!}</p>
</div>

<!-- Previous Profession Field -->
<div class="form-group">
    {!! Form::label('Previous_profession', 'Previous Profession:') !!}
    <p>{!! $storeOwner->Previous_profession !!}</p>
</div>

<!-- Pan Number Field -->
<div class="form-group">
    {!! Form::label('pan_number', 'Pan Number:') !!}
    <p>{!! $storeOwner->pan_number !!}</p>
</div>

<!-- Aadhar Number Field -->
<div class="form-group">
    {!! Form::label('aadhar_number', 'Aadhar Number:') !!}
    <p>{!! $storeOwner->aadhar_number !!}</p>
</div>

<!-- Account Num Field -->
<div class="form-group">
    {!! Form::label('account_num', 'Account Num:') !!}
    <p>{!! $storeOwner->account_num !!}</p>
</div>

<!-- Onboarding Status Field -->
<div class="form-group">
    {!! Form::label('onboarding_status', 'Onboarding Status:') !!}
    <p>{!! $storeOwner->onboarding_status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $storeOwner->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $storeOwner->updated_at !!}</p>
</div>

