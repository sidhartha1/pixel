@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Store Owner
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($storeOwner, ['route' => ['storeOwners.update', $storeOwner->id], 'method' => 'patch']) !!}

                        @include('store_owners.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection