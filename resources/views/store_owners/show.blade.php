@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Store Owner
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('store_owners.show_fields')
                    @if(user()->role=='admin')

                    <a href="{!! route('storeOwners.index') !!}" class="btn btn-default">Back</a>
                    @else
                        <a href="{!! route('home') !!}" class="btn btn-default">Back</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
