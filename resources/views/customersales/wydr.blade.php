<table class="table table-responsive" id="sales-table">
    <thead>
        <th>Customer Name</th>
        <th>Product Name</th>
        <th>Order Id</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Shipping</th>
        <th>Total</th>
        <th>Paid</th>
        <th>Balance</th>
        <th>Date & Time</th>

    </thead>

    <tbody>
    @foreach($wydr_sales as $sale)
        <tr>
            <td>{!! $sale->customer_name !!}</td>
            <td>{!! $sale->product_name !!}</td>
            <td>{!! $sale->order_id !!}</td>
            <td>{!! $sale->quantity !!}</td>
            <td>{!! $sale->price !!}</td>
            <td>{!! $sale->shipping_cost !!}</td>
            <td>{!! $sale->total_cost !!}</td>
            <td>{!! $sale->paid !!}</td>
            <td>{!! $sale->balance !!}</td>
            <td>{!! $sale->created_at->setTimezone(new DateTimeZone("Asia/Kolkata")) !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>
