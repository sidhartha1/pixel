@extends('layouts.app')

@section('content')

    @if($amazon_sales->count() > 0)
    <section class="content-header">
        <h1>Amazon Sales</h1></section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('customersales.amazon')
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($wydr_sales->count() > 0)
    <section class="content-header">
        <h1>Wydr Sales</h1></section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('customersales.wydr')
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($onemg_sales->count() > 0)
    <section class="content-header">
        <h1>Medicines Sales</h1></section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('customersales.onemg')
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($zestmoney_sales->count() > 0)
    <section class="content-header">
        <h1>ZestMoney Sales</h1></section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('customersales.zestmoney')
                </div>
            </div>
        </div>
    </div>
    @endif


@endsection
