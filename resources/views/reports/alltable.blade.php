@if(Auth::user()->role=='storeowner')
<table class="table table-responsive" id="reports-table">
    <thead>
        <th>Category</th>
        <th>Name</th>
        <th>Earning</th>
        <th>Items</th>
        <th>NewCustomerBonus</th>
        <th>DateShipped</th>
    </thead>
    <tbody>
        @foreach($all_reports as $all_report)
        <tr>
            <td>{!! $all_report->category !!}</td>
            <td>{!! $all_report->product_name !!}</td>
            <td>{!! $all_report->ad_fees !!}</td>
            <td>{!! $all_report->items_shipped !!}</td>
            <td>{!! $all_report->newcustomerbonus or 0 !!}</td>
            <td>{!! $all_report->date_shipped !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $all_reports->links() !!}
@endif