{{--<!-- Amazon Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amazon_store_id', 'Amazon Store Id:') !!}
    {!! Form::text('amazon_store_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ad Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ad_fees', 'Ad Fees:') !!}
    {!! Form::text('ad_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Nwecustomerbonus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nwecustomerbonus', 'Nwecustomerbonus:') !!}
    {!! Form::text('nwecustomerbonus', null, ['class' => 'form-control']) !!}
</div>--}}

<!-- Excel Sheet Field -->
<div class="form-group col-sm-6">
    {!! Form::label('excel_sheet', 'Excel Sheet:') !!}
    {!! Form::file('excel_sheet') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('reports.index') !!}" class="btn btn-default">Cancel</a>
</div>
