@if(Auth::user()->role=='admin')
<table class="table table-responsive" id="reports-table">
    <thead>
    <th>Id</th>
        <th>Amazon Store Id</th>
        <th>Ad Fees</th>
        <th>Newcustomerbonus</th>
        <th>Product Earnings</th>
        <th>TDS</th>
        <th>Net Earnings</th>
       {{-- <th>Excel Sheet</th>--}}
    @if(Auth::user()->role=='admin')
        <th colspan="3">Action</th>
        @endif
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td>{!! $report->id !!}</td>
            <td>{!! $report->amazon_store_id !!}</td>
            <td>{!! $report->ad_fees !!}</td>
            <td>{!! $report->newcustomerbonus !!}</td>
            <td>{!! round((($report->ad_fees + $report->newcustomerbonus)*0.9), 2) !!}</td>
            <td>{!! round((($report->ad_fees + $report->newcustomerbonus)*0.9)*0.05, 2) !!}</td>
            <td>{!! round((($report->ad_fees + $report->newcustomerbonus)*0.9)*0.95, 2) !!}</td>
           {{-- <td>{!! $report->excel_sheet !!}</td>--}}
            <td>
                {!! Form::open(['route' => ['reports.destroy', $report->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('reports.show', [$report->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('reports.edit', [$report->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $reports->links() !!}
@endif

@if(Auth::user()->role=='storeowner')
@foreach($reports as $report)
<div class="col-sm-6">
<table class="table table-responsive" id="reports-table">
    <thead>
        <th>Earnings</th>
        <th>Amount</th>
        <th>Deductions</th>
        <th>Amount</th>
    </thead>
    <tbody>
    <tr>
        <td>Product Earnings: </td>
        <td>&#8377; {!! round((($report->ad_fees + $report->newcustomerbonus)*0.9), 2) !!}</td>
        <td>TDS: </td>
        <td>&#8377; {!! round((($report->ad_fees + $report->newcustomerbonus)*0.9)*0.05, 2) !!}</td>
    </tr>
    <tr>
        <td>New Customer Bonus: </td>
        <td>&#8377; {!! $report->newcustomerbonus  or 0 !!}</td>
        <td>Other fees: </td>
        <td>&#8377; 0</td>
    </tr>
    <tr>
        <td>Total Earnings: </td>
        <td>&#8377; {!! round((($report->ad_fees + $report->newcustomerbonus)*0.9), 2) !!}</td>
        <td>Total Deductions: </td>
        <td>&#8377; {!! round((($report->ad_fees + $report->newcustomerbonus)*0.9)*0.05, 2) !!}</td>
    </tr>
    </tbody>
</table>
<h4><b>Net Earnings Payable:</b>  &#8377; {!! round((($report->ad_fees + $report->newcustomerbonus)*0.9)*0.95, 2) !!}</h4>
</div>
@endforeach
@endif