@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Earnings</h1>
        @if(Auth::user()->role=='admin')
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('reports.create') !!}">Add New</a>
            <hr>
        @endif
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('filter',['type'=>'today']) }}">Today</a>
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('filter',['type'=>'yesterday']) }}">Yesterday</a>
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('filter',['type'=>'week']) }}">Last Week</a>
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('filter',['type'=>'month']) }}">Last Month</a>
    </section>
    @if(Auth::user()->role=='admin')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <a href="{{ route('excel-file',['type'=>'xlsx']) }}">Download Excel File</a>
            {{--|
            <a href="{{ route('excel',['type'=>'xlsx']) }}">Download Last 10 Days Reports</a>--}}
        </div>
    </div>
    @endif
    <div class="content">
        <div class="clearfix"></div>
        <div class="pull-right" style="padding:5px;width:100%;text-align:right;" >
            <label>From:</label><input type="date" id="from-date">
            <label>To:</label><input type="date" id="to-date">
            <a class="btn btn-primary" onclick="date_filter()">Filter</a>
        </div>
        <div class="clearfix"></div>

        @include('flash::message')
        @if ($reports->count() > 0)
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('reports.table')
            </div>
        </div>
        @endif
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('reports.alltable')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    function date_filter() {
        var from = document.getElementById("from-date").value;
        var to = document.getElementById("to-date").value;
        var filter = from + ":" + to;
        var url = "{{ route('filter',['type'=>':filter']) }}";
        url = url.replace(':filter', filter);
        document.location.href = url;
    }
    </script>

@endsection
