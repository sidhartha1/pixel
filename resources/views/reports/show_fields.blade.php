<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $report->id !!}</p>
</div>

<!-- Amazon Store Id Field -->
<div class="form-group">
    {!! Form::label('amazon_store_id', 'Amazon Store Id:') !!}
    <p>{!! $report->amazon_store_id !!}</p>
</div>

<!-- Ad Fees Field -->
<div class="form-group">
    {!! Form::label('ad_fees', 'Ad Fees:') !!}
    <p>{!! $report->ad_fees !!}</p>
</div>

<!-- Nwecustomerbonus Field -->
<div class="form-group">
    {!! Form::label('nwecustomerbonus', 'Nwecustomerbonus:') !!}
    <p>{!! $report->newcustomerbonus !!}</p>
</div>

<!-- Excel Sheet Field -->
<div class="form-group">
    {!! Form::label('excel_sheet', 'Excel Sheet:') !!}
    <p>{!! $report->excel_sheet !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $report->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $report->updated_at !!}</p>
</div>

