<table class="table table-responsive" id="sales-table">
    <thead>

        <th>Customer Name</th>
        <th>Product</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Paid</th>
        <th>Balance</th>
        <th>Date & Time</th>
        @if(Auth::user()->role=='storeowner')
        <th colspan="3">Action</th>
            @endif
    </thead>
    <tbody>
    @foreach($sales as $sale)
        <tr>
            <td>{!! $sale->customer_name !!}</td>
            <td>{!! $sale->product_name !!}</td>
            <td>{!! $sale->product_quantity !!}</td>
            <td>{!! $sale->price !!}</td>
            <td>{!! $sale->paid !!}</td>
            <td>{!! $sale->balance !!}</td>
            <td>{!! $sale->created_at->setTimeZone(new DateTimeZone("Asia/Kolkata")) !!}</td>
            @if(Auth::user()->role=='storeowner')
            <td>
                {!! Form::open(['route' => ['sales.destroy', $sale->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('sales.edit', [$sale->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure you want to delete this order?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
                @endif
        </tr>
    @endforeach
    </tbody>
</table>
{!! $sales->links() !!}
