@extends('layouts.app')

@section('content')
   {{-- @if(Session::has('customer'))
        $customer=Session::get('customer');--}}

        <section class="content-header">
        <h1>
            Select the sales entry
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-block btn-warning" onclick="toggle_visibility('amazon')">Amazon Sales Entry</button>
                    <button type="button" class="btn btn-block btn-primary" onclick="toggle_visibility('wydr')">Wydr Sales Entry</button>
                    <!-- <button type="button" style=" background-color: #024b68; color:#ffffff;" class="btn btn-block" onclick="toggle_visibility('onemg')">Medicines Sales Entry</button> -->
                    <button type="button" class="btn btn-block btn-success" onclick="toggle_visibility('zestmoney')">ZestMoney Sales Entry</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('nosales')">No Sales</button>
                    <div class="clearfix"> <br> </div>
                </div>

            @if(Session::has('c_id'))

                <!-- Customer Name Field -->

                    <div class="form-group col-sm-6">
                        {!! Form::label('customer_name', 'Customer Name:') !!}
                        {!! Form::text('customer_name', Session::get('c_name'), ['class' => 'form-control','readonly']) !!}
                        <br>
                        <a href="{{ route('customersales',['type'=>Session::get('c_id')]) }}">Previous Orders of {{ Session::get('c_name') }}</a>
                        <div class="clearfix"> <br> </div>
                        <div class="form-group" id="nosales" style="display: none">
                            {!! Form::open(['route' => 'nosales']) !!}
                            {!! Form::text('customer_id', Session::get('c_id'), ['class' => 'form-control','style' => 'display:none']) !!}
                            {!! Form::text('customer_name', Session::get('c_name'), ['class' => 'form-control','style' => 'display:none']) !!}
                            {!! Form::label('reason_label', 'Reason for not buying:') !!}
						    {!! Form::select('reason', ['Did not find the required product' => 'Did not find the required product',
														'Just came to check the price' => 'Just came to check the price',
														'Found a lower price elsewhere' => 'Found a lower price elsewhere',
														'Stock not available' => 'Stock not available',
														'Not satisfied with the delivery time' => 'Not satisfied with the delivery time',
														'No trust in the product/service' => 'No trust in the product/service',
														'Still doing product research' => 'Still doing product research',
														'Other' => 'Other'], 1, ['class' => 'form-control']) !!}
							<br>
							{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
							{!! Form::close() !!}
                        </div>
                    </div>

                    </div>
                @endif
                 <div class="clearfix"></div>


                <div class="container">
                    <div class="row" id="amazon" style="display: none">
                        {!! Form::open(['route' => 'sales.store']) !!}
                        @include('sales.fields')

                        {!! Form::close() !!}
                    </div>

                    <div class="row" id="wydr" style="display: none">
                        {!! Form::open(['route' => 'wydr.store']) !!}
                        @include('wydr.fields')

                        {!! Form::close() !!}
                    </div>

                    <div class="row" id="onemg" style="display: none">
                        {!! Form::open(['route' => 'onemg.store']) !!}
                        @include('onemg.fields')

                        {!! Form::close() !!}
                    </div>

                    <div class="row" id="zestmoney" style="display: none">
                        {!! Form::open(['route' => 'zestmoney.store']) !!}
                        @include('zestmoney.fields')

                        {!! Form::close() !!}
                    </div>
                 </div>
            </div>
        </div>
    </div>
   {{-- @endif--}}
@endsection

@section('scripts')
    <script>
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        if (id == 'amazon') {
          amazonCalc();
          document.getElementById('wydr').style.display = 'none';
          document.getElementById('onemg').style.display = 'none';
          document.getElementById('zestmoney').style.display = 'none';
          document.getElementById('nosales').style.display = 'none';
        }else if (id == 'wydr') {
          document.getElementById('amazon').style.display = 'none';
          document.getElementById('onemg').style.display = 'none';
          document.getElementById('zestmoney').style.display = 'none';
          document.getElementById('nosales').style.display = 'none';
        }else if(id == 'onemg') {
          document.getElementById('wydr').style.display = 'none';
          document.getElementById('amazon').style.display = 'none';
          document.getElementById('zestmoney').style.display = 'none';
          document.getElementById('nosales').style.display = 'none';
        }else if(id == 'zestmoney') {
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('nosales').style.display = 'none';
        }else if(id == 'nosales') {
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('zestmoney').style.display = 'none';
        }
        if (e.style.display == 'block' || e.style.display=='') e.style.display = 'none';
        else e.style.display = 'block';
    }

    function amazonCalc(){
        $(document).ready(function() {
            $('#paid').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
            $('#price').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
//                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
            $('#product_quantity').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
//                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
        });
      }
    </script>
@endsection
