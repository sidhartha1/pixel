@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sale
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sale, ['route' => ['sales.update', $sale->id], 'method' => 'patch']) !!}

                        @include('sales.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#paid').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
            $('#price').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
//                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
            $('#product_quantity').on('input', function() {
                if(document.getElementById('price').value) {
                    var value = document.getElementById('price').value;
                    var qty = document.getElementById('product_quantity').value;
                    var value1 = document.getElementById('paid').value;
                    var value2 = document.getElementById('balance');
                    value2.value = (value*qty) - value1;
                }else{
//                    alert("Please Enter the price of product");
                    document.getElementById('balance').value = "";
                }
            });
        });
    </script>
@endsection