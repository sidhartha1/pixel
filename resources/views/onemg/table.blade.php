<table class="table table-responsive" id="sales-table">
    <thead>
    <th>Customer Name</th>
    <th>Order Id</th>
    <th>Price</th>
    <th>Paid</th>
    <th>Balance</th>
    <th>Date & Time</th>

    </thead>

    <tbody>
    @foreach($onemg_sales as $sale)
        <tr>
            <td>{!! $sale->customer_name !!}</td>
            <td>{!! $sale->order_id !!}</td>
            <td>{!! $sale->order_value !!}</td>
            <td>{!! $sale->paid !!}</td>
            <td>{!! $sale->balance !!}</td>
            <td>{!! $sale->created_at->setTimeZone(new DateTimeZone("Asia/Kolkata")) !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>
