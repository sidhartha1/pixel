@if(Session::has('c_id'))
<!-- Customer Id Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', Session::get('c_id'), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:none">
        {!! Form::label('customer_name', 'Customer Name:') !!}
        {!! Form::text('customer_name', Session::get('c_name'), ['class' => 'form-control','readonly']) !!}
</div>
@endif

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_value', 'Price:') !!}
    {!! Form::number('order_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Paid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paid', 'Paid:') !!}
    {!! Form::number('paid', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field
<div class="form-group col-sm-6">
    {!! Form::label('balance', 'Balance:') !!}
    {!! Form::text('balance', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::datetime('date', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d'), ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(user()->role=="storeowner")
        <a href="{!! route('home') !!}" class="btn btn-default"> Cancel</a>
    @else
        <a href="{!! route('onemg.sales') !!}" class="btn btn-default">Cancel</a>
    @endif

</div>
