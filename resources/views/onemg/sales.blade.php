@extends('layouts.app')

@section('content')


    <section class="content-header">
        <h1 class="pull-left">Medicines Sales</h1>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('onemgfilter',['type'=>'today']) }}">Today</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('onemgfilter',['type'=>'yesterday']) }}">Yesterday</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('onemgfilter',['type'=>'week']) }}">Last Week</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('onemgfilter',['type'=>'month']) }}">Last Month</a>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('onemg.table')
                </div>
            </div>
        </div>
    </div>


@endsection
