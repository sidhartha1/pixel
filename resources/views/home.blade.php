@extends('layouts.app')
@section('content')
@if(user()->role=="admin")
{
}
@else
<div class="content">
   @include('adminlte-templates::common.errors')
   <div class="box box-primary">
      <div class="box-body">
         <div class="row">
            <div class="col-md-12">
               <div class="box box-solid">
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                           <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                           <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                           <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                        </ol>
                        <div class="carousel-inner">
                           <div class="item">
                              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img18/Wireless/CEEX/OnePlus6/OPWeekApril/pcREVISED1500X300._CB475007632_.jpg" alt="First slide">
                           </div>
                           <div class="item">
                              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img18/PC/Printer/1118933print-more_1500x300._CB477733486_.jpg" alt="Second slide">
                           </div>
                           <div class="item active">
                              <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img16/Grocery/Offers/9585-GW-Hero-Grocery-Offers-1x._CB510912218_.jpg" alt="Third slide">
                           </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                        </a>
                     </div>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->
            </div>
         </div>

      </div>
   </div>


   <!-- Customer Number -->
   <div class="row">
      {!! Form::open(['route' => 'checkCustomer']) !!}
      <!-- Phone Field -->
      <div class="form-group col-sm-3">
         {!! Form::label('phone', 'Enter Customer Phone No:') !!}
         {!! Form::text('phone', null, ['class' => 'form-control']) !!}
      </div>
      <div class="form-group col-sm-12">
         {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
      </div>
      {!! Form::close() !!}
   </div>
   <!-- Customer Number -->

</div>

@endif
@endsection
