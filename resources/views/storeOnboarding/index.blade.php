@extends('layouts.app')

@section('content')
   {{-- @if(Session::has('customer'))
        $customer=Session::get('customer');--}}

        <section class="content-header">
        <h1>
            Store Onboarding
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body" style="margin-bottom:20px;">
                <h4 style="text-align: center"><b>Onboarding</b></h4>
                <div class="onb-progress">
                  <div class="circle done" onclick="window.location='{{ URL::route('storeOwners.show',user()->id)}}'">
                    <span class="label">1</span>
                    <span class="title">Registration</span>
                  </div>
                  <span class="bar done"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">2</span>
                    <span class="title">TokenPayment</span>
                  </div>
                  <span class="bar active"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">3</span>
                    <span class="title">TokenApproval</span>
                  </div>
                  <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">4</span>
                    <span class="title">DeepKYC</span>
                  </div>
                  <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">5</span>
                    <span class="title">StoreKYC</span>
                  </div>
                  <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">6</span>
                    <span class="title">FullPayment</span>
                  </div>
                    <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">7</span>
                    <span class="title">PaymentApproval</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">8</span>
                    <span class="title">DesignCoordination</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">9</span>
                    <span class="title">UploadStorePics</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">10</span>
                    <span class="title">StoreApproval</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">11</span>
                    <span class="title">TakeTests</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle done" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">12</span>
                    <span class="title">FinalApproval</span>
                  </div>
                </div>

                <h4 style="text-align: center"><b>Videos</b></h4>
                <div class="vid-progress">
                  <div class="circle active" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">1</span>
                    <span class="title">Video1</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">2</span>
                    <span class="title">Video2</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">3</span>
                    <span class="title">Video3</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">4</span>
                    <span class="title">Video4</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">5</span>
                    <span class="title">Video5</span>
                  </div>
                </div>

                <h4 style="text-align: center"><b>Tests</b></h4>
                <div class="test-progress">
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">1</span>
                    <span class="title">test1</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">2</span>
                    <span class="title">test2</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">3</span>
                    <span class="title">test3</span>
                  </div>
                      <span class="bar"></span>
                  <div class="circle" onclick="window.location='{{ URL::route('home') }}'">
                    <span class="label">4</span>
                    <span class="title">test4</span>
                  </div>
                </div>
            </div>
        </div>
    </div>
   {{-- @endif--}}
@endsection

@section('scripts')
    <script>
        var states = {{ $state }};
        $('.onb-progress .circle').removeClass().addClass('circle');
        $('.onb-progress .bar').removeClass().addClass('bar');
        for (i=1; i<=states; i++){
            if (i==states) {
                update(i, true);
            } else {
                update(i, false);
            }
        }

        function update(i, is_current) {
          
          $('.onb-progress .circle:nth-of-type(' + i + ')').addClass('active');
          
          $('.onb-progress .circle:nth-of-type(' + (i-1) + ')').addClass('done').removeClass('active');
          
          $('.onb-progress .circle:nth-of-type(' + (i-1) + ') .label').html('&#10003;');
          
          $('.onb-progress .bar:nth-of-type(' + (i-1) + ')').addClass('active');
          
          $('.onb-progress .bar:nth-of-type(' + (i-2) + ')').addClass('done').removeClass('active');
          
          if (i==0) {
              $('.onb-progress .bar').removeClass().addClass('bar');
              $('.onb-progress div.circle').removeClass().addClass('circle');
          }
        }
    </script>
@endsection
