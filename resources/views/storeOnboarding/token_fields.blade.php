<!-- Customer Id Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('storeowner_name', 'Name:') !!}
    {!! Form::text('storeowner_name', Auth::user->name(), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('storeowner_email', 'Email:') !!}
    {!! Form::text('storeowner_email', Auth::user->email(), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('storeowner_phone', 'Mobile:') !!}
    {!! Form::text('storeowner_phone', Auth::user->phone(), ['class' => 'form-control','readonly']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::datetime('date', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d H:m:s'), ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(user()->role=="storeowner")
        <a href="{!! route('storeOnboarding.index') !!}" class="btn btn-default"> Cancel</a>
    @else
        <a href="{!! route('storeOnboarding.index') !!}" class="btn btn-default">Cancel</a>
    @endif
</div>
