@extends('layouts.app')

@section('content')
    @if(Session::has('customer'))
         $customer=Session::get('customer');
    @endif
    <section class="content-header">
        <h1>
            Loanyantra Loan Entry
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'storeOnboarding.processtoken']) !!}
                    @include('storeOnboarding.token_fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
