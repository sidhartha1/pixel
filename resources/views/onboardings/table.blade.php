<table class="table table-responsive" id="onboardings-table">
    <thead>
    <th>Id</th>
        <th>Name</th>
        <th>Url</th>
        <th>Question</th>
        <th>Option 1</th>
        <th>Option 2</th>
        <th>Option 3</th>
        <th>Option 4</th>
        <th>Correct Ans</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($onboardings as $onboarding)
        <tr>
            <td>{!! $onboarding->id !!}</td>
            <td>{!! $onboarding->name !!}</td>
            <td>{!! $onboarding->url !!}</td>
            <td>{!! $onboarding->question !!}</td>
            <td>{!! $onboarding->option_1 !!}</td>
            <td>{!! $onboarding->option_2 !!}</td>
            <td>{!! $onboarding->option_3 !!}</td>
            <td>{!! $onboarding->option_4 !!}</td>
            <td>{!! $onboarding->correct_ans !!}</td>
            <td>
                {!! Form::open(['route' => ['onboardings.destroy', $onboarding->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('onboardings.show', [$onboarding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('onboardings.edit', [$onboarding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $onboardings->links() !!}