<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $onboarding->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $onboarding->name !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $onboarding->url !!}</p>
</div>

<!-- Question Field -->
<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    <p>{!! $onboarding->question !!}</p>
</div>

<!-- Option 1 Field -->
<div class="form-group">
    {!! Form::label('option_1', 'Option 1:') !!}
    <p>{!! $onboarding->option_1 !!}</p>
</div>

<!-- Option 2 Field -->
<div class="form-group">
    {!! Form::label('option_2', 'Option 2:') !!}
    <p>{!! $onboarding->option_2 !!}</p>
</div>

<!-- Option 3 Field -->
<div class="form-group">
    {!! Form::label('option_3', 'Option 3:') !!}
    <p>{!! $onboarding->option_3 !!}</p>
</div>

<!-- Oprtion 4 Field -->
<div class="form-group">
    {!! Form::label('option_4', 'Option 4:') !!}
    <p>{!! $onboarding->option_4 !!}</p>
</div>

<!-- Correct Ans Field -->
<div class="form-group">
    {!! Form::label('correct_ans', 'Correct Ans:') !!}
    <p>{!! $onboarding->correct_ans !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $onboarding->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $onboarding->updated_at !!}</p>
</div>

