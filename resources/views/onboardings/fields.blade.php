<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Option 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_1', 'Option 1:') !!}
    {!! Form::text('option_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Option 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_2', 'Option 2:') !!}
    {!! Form::text('option_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Option 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_3', 'Option 3:') !!}
    {!! Form::text('option_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Oprtion 4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_4', 'Option 4:') !!}
    {!! Form::text('option_4', null, ['class' => 'form-control']) !!}
</div>

<!-- Correct Ans Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correct_ans', 'Correct Ans:') !!}
    {!! Form::text('correct_ans', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('onboardings.index') !!}" class="btn btn-default">Cancel</a>
</div>
