<?php
function freshdesk_login_url($strName, $strEmail) {
	$secret = '7fb6a27571209f4f426f1c10b81e6f65';
	$base = 'https://ibpixel.freshdesk.com/';
	$redirect = 'https://ibpixel.freshdesk.com/support/tickets/#hc-search-form';
	$timestamp = time();
	$to_be_hashed = $strName . $secret . $strEmail . $timestamp;
	$hash = hash_hmac('md5', $to_be_hashed, $secret);

	return $base ."login/sso/?name=".urlencode($strName)."&email=".urlencode($strEmail)."&timestamp=".$timestamp."&hash=".$hash."&redirect_to=" . $redirect;
}

?>

@extends('layouts.app')

@section('content')
	<div class="content-header">
        <h1 id="create_ticket" class="pull-left">Create New Ticket</h1>
        <h1 id="view_tickets" style="display:none" class="pull-left">All Tickets</h1>
        <a id="create_ticket_btn" style="display:none"  class="btn btn-primary pull-right" onclick="toggle_tickets('create_ticket')" style="margin-bottom:-5px; margin-right: 5px">Create New Ticket</a>
        <a id="view_tickets_btn" class="btn btn-primary pull-right" style="margin-bottom:-5px; margin-right: 5px" onclick="toggle_tickets('view_tickets')">View All Tickets</a>
        <div class="clearfix"></div>
    </div>

	<div class="content">
           <div class="box box-primary" id="create_ticket_box" >

               <div class="box-body">
               <div class="col-sm-6">
					{!! Form::open(['route' => 'submitTicket']) !!}
                   <div class="row">
						<div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px;" onclick="toggle_visibility('amazon')">
		                    <i class="fa fa-amazon fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Amazon</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
						<div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('wydr')">
							<i class="fa fa-wikipedia-w fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Wydr</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
						<div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('medicines')">
							<i class="fa fa-medkit fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Medicines</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
					</div>

                   <div class="row">
                       <div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('indiabuys')">
							<i class="fa fa-shopping-cart fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">IndiaBuys</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
                       <div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('loans')">
							<i class="fa fa-money fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Loans</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
                       <div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('zest')">
							<i class="fa fa-money fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Zest Money EMI</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
                      </div>

					<div class="row">
                       <div class="col-sm-4">
                            <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('ibpay')">
                            <i class="fa fa-credit-card fa-2x"></i><br>
                            <p style="font-size:18px;white-space:normal">ibPay</p>
                            </button>
                        <div class="clearfix"> <br> </div>
                        </div>
                       <div class="col-sm-4">
		                    <button type="button" class="btn btn-block btn-primary" style="width:125px;height:125px" onclick="toggle_visibility('pixel')">
							<i class="fa fa-headphones fa-2x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Pixel</p>
		                    </button>
		                <div class="clearfix"> <br> </div>
                		</div>
                      </div>
                       {!! Form::close() !!}
                   </div>
                   <div class="col-sm-6">
                   <div class="container col-sm-12">
	                    <div class="row" id="amazon" style="display: none">
	                        {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.amazon')
	
	                        {!! Form::close() !!}
	                    </div>
	
	                    <div class="row" id="wydr" style="display: none">
	                        {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.wydr')
	
	                        {!! Form::close() !!}
	                    </div>
	
	                    <div class="row" id="medicines" style="display: none">
	                       {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.medicines')
	
	                        {!! Form::close() !!}
	                    </div>
	                    
	                    <div class="row" id="indiabuys" style="display: none">
	                        {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.indiabuys')
	
	                        {!! Form::close() !!}
	                    </div>
	
	                    <div class="row" id="loans" style="display: none">
	                       {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.loans')
	
	                        {!! Form::close() !!}
	                    </div>
	
	                    <div class="row" id="zest" style="display: none">
	                       {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.zestmoney')
	
	                        {!! Form::close() !!}
	                    </div>
	                    
	                    <div class="row" id="ibpay" style="display: none">
	                       {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.ibpay')
	
	                        {!! Form::close() !!}
	                    </div>

	                    <div class="row" id="pixel" style="display: none">
	                       {!! Form::open(['route' => 'submitTicket']) !!}
	
	                        @include('support.pixel')
	
	                        {!! Form::close() !!}
	                    </div>
                 </div>
                 </div>
               </div>
           </div>
           <div class="box box-primary" id="view_tickets_box" style="flex-grow: 1; flex-direction: column; display: none;">
               <div class="box-body" style="flex-grow: 1; flex-direction: column; display: flex;">
                    <iframe style="flex-grow: 1; width: 100%" src=<?php echo(freshdesk_login_url(Auth::user()->name, Auth::user()->email));?> frameborder="0" allowfullscreen></iframe>
               </div>
           </div>
	</div>


@endsection

@section('scripts')
    <script>
    function toggle_tickets(str) {
        var ct = 'create_ticket';
        var vt = 'view_tickets';
        if (str == ct)
        {
            document.getElementById(ct).style.display ='block';
            document.getElementById(ct + '_btn').style.display = 'none';
            document.getElementById(ct + '_box').style.display = 'block';
            document.getElementById(vt).style.display ='none';
            document.getElementById(vt + '_btn').style.display = 'block';
            document.getElementById(vt + '_box').style.display = 'none';
        }
        if (str == vt)
        {
            document.getElementById(ct).style.display ='none';
            document.getElementById(ct + '_btn').style.display = 'block';
            document.getElementById(ct + '_box').style.display = 'none';
            document.getElementById(vt).style.display ='block';
            document.getElementById(vt + '_btn').style.display = 'none';
            document.getElementById(vt + '_box').style.display = 'flex';
        }
    }
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        console.log(e);
        if (id == 'amazon') {
          document.getElementById('wydr').style.display = 'none';
          document.getElementById('medicines').style.display = 'none';
          document.getElementById('indiabuys').style.display = 'none';
          document.getElementById('zest').style.display = 'none';
          document.getElementById('pixel').style.display = 'none';
          document.getElementById('ibpay').style.display = 'none';
          document.getElementById('loans').style.display = 'none';
        }else if (id == 'wydr') {
          document.getElementById('amazon').style.display = 'none';
          document.getElementById('medicines').style.display = 'none';
          document.getElementById('indiabuys').style.display = 'none';
          document.getElementById('zest').style.display = 'none';
          document.getElementById('pixel').style.display = 'none';
          document.getElementById('ibpay').style.display = 'none';
          document.getElementById('loans').style.display = 'none';
        }else if(id == 'medicines') {
          document.getElementById('amazon').style.display = 'none';
          document.getElementById('wydr').style.display = 'none';
          document.getElementById('indiabuys').style.display = 'none';
          document.getElementById('zest').style.display = 'none';
          document.getElementById('pixel').style.display = 'none';
          document.getElementById('ibpay').style.display = 'none';
          document.getElementById('loans').style.display = 'none';
        }else if(id == 'indiabuys') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('medicines').style.display = 'none';
            document.getElementById('zest').style.display = 'none';
            document.getElementById('pixel').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('loans').style.display = 'none';
        }else if(id == 'loans') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('medicines').style.display = 'none';
            document.getElementById('indiabuys').style.display = 'none';
            document.getElementById('zest').style.display = 'none';
            document.getElementById('pixel').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
        }else if(id == 'zest') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('medicines').style.display = 'none';
            document.getElementById('indiabuys').style.display = 'none';
            document.getElementById('pixel').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('loans').style.display = 'none';
        }else if(id == 'pixel') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('medicines').style.display = 'none';
            document.getElementById('indiabuys').style.display = 'none';
            document.getElementById('zest').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('loans').style.display = 'none';
        }else if(id == 'ibpay') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('medicines').style.display = 'none';
            document.getElementById('indiabuys').style.display = 'none';
            document.getElementById('zest').style.display = 'none';
            document.getElementById('pixel').style.display = 'none';
            document.getElementById('loans').style.display = 'none';
        }
        if (e.style.display == 'block' || e.style.display=='') e.style.display = 'none';
        else e.style.display = 'block';
    }
    </script>
    
@endsection