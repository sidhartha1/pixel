<table class="table table-responsive" id="sales-table">
    <thead>
        <th>Rx Id</th>
        <th>Customer Name</th>
        <th>Order Id</th>
        <th>Order State</th>
        <th>Est. Delivery</th>
        <th>Total Amount</th>
        <th>Payable Amount</th>
        <th>Images Uploaded</th>
        <th>Pincode</th>
        <th>Date & Time</th>

    </thead>

    <tbody>
    @foreach($medlife_orders as $sale)
        <tr>
            <td>{!! $sale->rx_id !!}</td>
            <td>{!! $sale->customer_name !!}</td>
            <td>{!! $sale->order_id !!}</td>
            <td>{!! $sale->order_state !!}</td>
            <td>{!! $sale->estimated_delivery_date !!}</td>
            <td>{!! $sale->total_amount !!}</td>
            <td>{!! $sale->payable_amount !!}</td>
            <td>{!! $sale->upload_count !!}</td>
            <td>{!! $sale->delivery_pincode !!}</td>
            <td>{!! $sale->created_at->setTimeZone(new DateTimeZone("Asia/Kolkata")) !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>
