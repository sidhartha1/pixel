@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>Medlife Orders</h1>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('medlifefilter',['type'=>'today']) }}">Today</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('medlifefilter',['type'=>'yesterday']) }}">Yesterday</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('medlifefilter',['type'=>'week']) }}">Last Week</a>
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px; margin-right: 5px" href="{{ route('medlifefilter',['type'=>'month']) }}">Last Month</a>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="pull-right" style="padding:5px;width:100%;text-align:right;" >
            <label>From:</label><input type="date" id="from-date">
            <label>To:</label><input type="date" id="to-date">
            <a class="btn btn-primary" onclick="date_filter()">Filter</a>
        </div>
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('medlife.table')
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
    function date_filter() {
        var from = document.getElementById("from-date").value;
        var to = document.getElementById("to-date").value;
        var filter = from + ":" + to;
        var url = "{{ route('medlifefilter',['type'=>':filter']) }}";
        url = url.replace(':filter', filter);
        document.location.href = url;
    }
    </script>

@endsection
