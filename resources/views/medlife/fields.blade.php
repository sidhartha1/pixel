@if(Session::has('c_id'))

<!-- Customer Id Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', Session::get('c_id'), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:block">
        {!! Form::label('customer_name', 'Customer Name:') !!}
        {!! Form::text('customer_name', Session::get('c_name'), ['class' => 'form-control','readonly']) !!}
</div>
<div class="form-group col-sm-6" style="display:none">
        {!! Form::label('customer_phone', 'Customer Mobile:') !!}
        {!! Form::text('customer_phone', Session::get('c_phone'), ['class' => 'form-control','readonly']) !!}
</div>
@endif

<!-- Image Upload -->
<div class="form-group col-sm-12">
     <br>
    {!! Form::label('upload_image', 'Prescription (Upload Images)') !!}
    {!! Form::file('upload_image[]', ['class' => 'form-control', 'multiple', 'required']) !!}
</div>

<!-- Delivery Label -->
<div class="col-sm-12">
    <br>
    {!! Form::label('deliver', 'Delivery Address') !!}
</div>

<!-- Delivery Name -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_name', 'Name:') !!}
    {!! Form::text('delivery_name', Session::get('c_name'), ['class' => 'form-control', 'required']) !!}
</div>

<!-- Delivery Mobile -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_mobile', 'Mobile:') !!}
    {!! Form::text('delivery_mobile', Session::get('c_phone'), ['class' => 'form-control', 'required']) !!}
</div>

<!-- Delivery Address Line 1 -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_add_1', 'Address Line 1:') !!}
    {!! Form::text('delivery_add_1', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Delivery Address Line 2 -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_add_2', 'Address Line 2:') !!}
    {!! Form::text('delivery_add_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery City -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_city', 'City:') !!}
    {!! Form::text('delivery_city', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Delivery Pincode -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_pincode', 'Pincode:') !!}
    {!! Form::text('delivery_pincode', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::datetime('date', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d H:m:s'), ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(user()->role=="storeowner")
        <a href="{!! route('home') !!}" class="btn btn-default"> Cancel</a>
    @else
        <a href="{!! route('home') !!}" class="btn btn-default">Cancel</a>
    @endif
</div>
