@extends('layouts.app')

@section('content')
    @if(Session::has('customer'))
         $customer=Session::get('customer');
    @endif
    <section class="content-header">
        <a class="btn btn-primary pull-right" style="padding: 10px; margin-right: 5px" href="{{ url('medlife') }}">View Medicine Orders</a>
        <h1>
            Medlife Order Entry
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'medlife.store', 'files' => true]) !!}
                    @include('medlife.fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
