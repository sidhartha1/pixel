@extends('layouts.app')

@section('content')
   {{-- @if(Session::has('customer'))
        $customer=Session::get('customer');--}}

        <section class="content-header">
        <h1>
            Select the sales
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-block btn-warning" onclick="window.location='{{ route('sales.index') }}'">Amazon Sales</button>
                    <button type="button" class="btn btn-block btn-warning" onclick="window.location='{{ route('orders.index') }}'">Amazon Tracked Orders</button>
                    <button type="button" class="btn btn-block btn-success" onclick="window.location='{{ route('zestmoney.index') }}'">ZestMoney Sales</button>
                    <div class="clearfix"> <br> </div>
                </div>
            </div>
        </div>
    </div>
   {{-- @endif--}}
@endsection

@section('scripts')
    <script>
    </script>
@endsection
