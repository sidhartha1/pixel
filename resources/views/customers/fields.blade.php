{{--<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::text('owner_id', null, ['class' => 'form-control']) !!}
</div>--}}

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

@if(Session::has('phone'))

    <!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Phone:') !!}
        {!! Form::number('phone', Session::get('phone'), ['class' => 'form-control','readonly']) !!}
    </div>
@else
    <!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Phone:') !!}
        {!! Form::number('phone', null, ['class' => 'form-control']) !!}
    </div>
@endif

<div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-6">
    {!! Form::label('age', 'Age:') !!}
    {!! Form::number('age', null, ['class' => 'form-control']) !!}

</div>


<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!} <br>
    {!! Form::radio('gender', 'Male', true) !!} Male
    {!! Form::radio('gender', 'Female', false) !!} Female
</div>

<!-- Profession Field -->
<div class="form-group col-sm-6">
    {!! Form::label('profession', 'Profession:') !!}
    {!! Form::select('profession', ['Student'=>'Student', 'Govt Employee'=>'Govt Employee', 'Private Employee'=>'Private Employee', 'Businessman'=>'Businessman', 'House Wife'=>'House Wife', 'Others'=>'Other'], 'Student', ['class' => 'form-control']) !!}
</div>

<!-- Source Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::select('source', ['Store Walkin'=>'Store Walkin',
                                'Word of mouth/Referral'=>'Word of mouth/Referral',
                                'Pamphlet'=>'Pamphlet',
                                'WhatsApp'=>'WhatsApp',
                                'SMS Campaign'=>'SMS Campaign',
                                'Billboards'=>'Billboards',
                                'Other'=>'Other'], 'Store Walkin', ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'readonly']) !!}
</div>
<!-- Time Field -->
<div class="form-group col-sm-6" style="display:none">
    {!! Form::label('time', 'Time:') !!}
    {!! Form::time('time', \Carbon\Carbon::now(new DateTimeZone('Asia/Kolkata'))->format('H:i'), ['class' => 'form-control', 'readonly']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Add Customer', ['class' => 'btn btn-primary']) !!}

    @if(user()->role=="storeowner")
    <a href="{!! route('home') !!}" class="btn btn-default"> Cancel</a>
        @else
        <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
    @endif

</div>
