<table class="table table-responsive" id="customers-table">
    <thead>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Age</th>
        <th>Gender</th>
        <th>Profession</th>
        <th>Source</th>
        <th>Date</th>
        <th>Time</th>
    @if(Auth::user()->role=='admin')
        <th colspan="3">Action</th>
        @endif
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{!! $customer->name !!}</td>
            <td>{!! $customer->phone !!}</td>
            <td>{!! $customer->email !!}</td>
            <td>{!! $customer->age !!}</td>
            <td>{!! $customer->gender !!}</td>
            <td>{!! $customer->profession !!}</td>
            <td>{!! $customer->source !!}</td>
            <td>{!! $customer->date->format('d-m-Y') !!}</td>
            <td>{!! $customer->time !!}</td>
            @if(Auth::user()->role=='admin')
            <td>
                {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('customers.show', [$customer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('customers.edit', [$customer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
                @endif
        </tr>
    @endforeach
    </tbody>
</table>
{!! $customers->links() !!}
