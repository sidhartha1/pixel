<!-- Store Field (Amazon,Wydr,Medicines,IndiaBuys,ZestMoney EMI,Pixel,ibPay) -->
<div class="form-group col-sm-12">
    {!! Form::label('store_label', 'Category:') !!}
    {!! Form::text('store', 'Amazon', ['class' => 'form-control', 'readonly']) !!}
</div>
<br>

<!-- Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issue_label', 'Issue:') !!}
    {!! Form::select('issue', ['Issue with Delivery People' => 'Issue with Delivery People',
                              'Placing an Order' =>'Placing an Order',
                              'Order not tracked' =>'Order not tracked',
                              'Delivery related Issue' =>'Delivery related Issue',
                              'Returns and Exchanges' =>'Returns and Exchanges',
                              'Order Payment Issues' =>'Order Payment Issues',
                              'Earnings' =>'Earnings',
                              'When will I get my payment' =>'When will I get my payment',
                              'Other' =>'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_label', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>