<!-- Store Field (Amazon,Wydr,Medicines,IndiaBuys,Loans,ZestMoney EMI,Pixel,ibPay) -->
<div class="form-group col-sm-12">
    {!! Form::label('store_label', 'Category:') !!}
    {!! Form::text('store', 'Loans', ['class' => 'form-control', 'readonly']) !!}
</div>
<br>

<!-- Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issue_label', 'Issue:') !!}
    {!! Form::select('issue', ['How to Apply for a Loan' => 'How to Apply for a Loan',
                              'Update on the Loan status' =>'Update on the Loan status',
                              'Problem with an Application' =>'Problem with an Application',
                              'Commission Structure' =>'Commission Structure',
                              'Other' =>'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_label', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>