<!-- Store Field (Amazon,Wydr,Medicines,IndiaBuys,ZestMoney EMI,Pixel,ibPay) -->
<div class="form-group col-sm-12">
    {!! Form::label('store_label', 'Category:') !!}
    {!! Form::text('store', 'ibPay', ['class' => 'form-control', 'readonly']) !!}
</div>
<br>

<!-- Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issue_label', 'Issue:') !!}
    {!! Form::select('issue', ['Recharges' => 'Recharges',
    									  'Flights' => 'Flights',
    									  'Bus' => 'Bus',
    									  'DTH Connection' => 'DTH Connection',
    									  'Utility' => 'Utility',
    									  'Money Transfer' => 'Money Transfer',
    									  'Railway' => 'Railway',
    									  'PAN Card' => 'PAN Card',
    									  'Other' => 'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_label', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>