<!-- Store Field (Amazon,Wydr,Medicines,IndiaBuys,ZestMoney EMI,Pixel,ibPay) -->
<div class="form-group col-sm-12">
    {!! Form::label('store_label', 'Category:') !!}
    {!! Form::text('store', 'Medicines', ['class' => 'form-control', 'readonly']) !!}
</div>
<br>

<!-- Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issue_label', 'Issue:') !!}
    {!! Form::select('issue', ['Prescription related issue' => 'Prescription related issue',
                              'Medicines not available' => 'Medicines not available',
                              'Delivery related Issue' => 'Delivery related Issue',
                              'Placing an Order' => 'Placing an Order',
                              'Rerturns & Exchanges' => 'Rerturns & Exchanges',
                              'Order Payment Issues' => 'Order Payment Issues',
                              'Earnings' => 'Earnings',
                              'When will I get my payment' => 'When will I get my payment',
                              'Other' => 'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_label', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>