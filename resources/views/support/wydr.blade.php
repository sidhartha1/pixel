<!-- Store Field (Amazon,Wydr,Medicines,IndiaBuys,ZestMoney EMI,Pixel,ibPay) -->
<div class="form-group col-sm-12">
    {!! Form::label('store_label', 'Category:') !!}
    {!! Form::text('store', 'Wydr', ['class' => 'form-control', 'readonly']) !!}
</div>
<br>

<!-- Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issue_label', 'Issue:') !!}
    {!! Form::select('issue', ['Negotiate a Better Price' => 'Negotiate a Better Price',
                                'Placing an Order' => 'Placing an Order',
                                'Delivery related issue' => 'Delivery related issue',
                                'Returns and Exchanges' => 'Returns and Exchanges',
                                'Order Payment Issues' => 'Order Payment Issues',
                                'Order Tracking' => 'Order Tracking',
                                'Offers in Wydr' => 'Offers in Wydr',
                                'Earnings' => 'Earnings',
                                'When will I get my payment' => 'When will I get my payment',
                                'Other' => 'Other'], 1, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_label', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
</div>