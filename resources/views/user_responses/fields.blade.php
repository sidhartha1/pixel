<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::text('owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Question Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question_id', 'Question Id:') !!}
    {!! Form::text('question_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Answer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_answer', 'User Answer:') !!}
    {!! Form::text('user_answer', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('userResponses.index') !!}" class="btn btn-default">Cancel</a>
</div>
