<table class="table table-responsive" id="userResponses-table">
    <thead>
    <th>Id</th>
        <th>Owner Id</th>
        <th>Question Id</th>
        <th>User Answer</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($userResponses as $userResponse)
        <tr>
            <td>{!! $userResponse->id !!}</td>
            <td>{!! $userResponse->owner_id !!}</td>
            <td>{!! $userResponse->question_id !!}</td>
            <td>{!! $userResponse->user_answer !!}</td>
            <td>
                {!! Form::open(['route' => ['userResponses.destroy', $userResponse->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('userResponses.show', [$userResponse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('userResponses.edit', [$userResponse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{!! $userResponses->links() !!}