@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            User Response
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($userResponse, ['route' => ['userResponses.update', $userResponse->id], 'method' => 'patch']) !!}

                        @include('user_responses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection