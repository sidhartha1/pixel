<table class="table table-responsive" id="orders-table">
    <thead>
        <tr>
            <th>Item</th>
        <th>Asin</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Date</th>
        <!--    <th colspan="3">Action</th> -->
        </tr>
    </thead>
    <tbody>
    @foreach($orders->sortBy('date')  as $order)
        <tr>
            <td>{!! $order->item !!}</td>
            <td>{!! $order->ASIN !!}</td>
            <td>{!! $order->Quantity !!}</td>
            <td>{!! $order->Price !!}</td>
            <td>{!! $order->date->format("d-m-Y") !!}</td>
<!--
            <td>
                {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('orders.show', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('orders.edit', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td> -->
        </tr>
    @endforeach
    </tbody>
</table>
{!! $orders->links() !!}
