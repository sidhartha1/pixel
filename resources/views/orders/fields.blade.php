<div class="form-group col-sm-6">
    {!! Form::label('excel_sheets', 'Excel Sheet:') !!}
    {!! Form::file('excel_sheets') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orders.index') !!}" class="btn btn-default">Cancel</a>
</div>