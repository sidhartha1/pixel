<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $order->id !!}</p>
</div>

<!-- Item Field -->
<div class="form-group">
    {!! Form::label('item', 'Item:') !!}
    <p>{!! $order->item !!}</p>
</div>

<!-- Asin Field -->
<div class="form-group">
    {!! Form::label('ASIN', 'Asin:') !!}
    <p>{!! $order->ASIN !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('Quantity', 'Quantity:') !!}
    <p>{!! $order->Quantity !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('Price', 'Price:') !!}
    <p>{!! $order->Price !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $order->date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $order->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $order->updated_at !!}</p>
</div>
