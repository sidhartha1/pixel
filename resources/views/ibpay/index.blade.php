@extends('layouts.app')

@section('content')
	<div class="content-header">
        <h1 id="create_ticket" class="pull-left">ibPay</h1>
        <div class="clearfix"></div>
    </div>

	<div class="content">
           <div class="box box-primary" id="create_ticket_box" >

               <div class="box-body">
               <div class="col-sm-6">
                   <div class="row">
						<div class="col-sm-6">
		                    <a href="http://agent.ibpay.in" target="_blank">
		                    <button type="button" class="btn btn-block btn-primary" style="width:200px;height:200px;">
		                    <i class="fa fa-black-tie fa-3x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Agent Login</p>
		                    </button>
		                    </a>
		                <div class="clearfix"> <br> </div>
                		</div>
						<div class="col-sm-6">
		                    <a href="http://distributor.ibpay.in" target="_blank">
		                    <button type="button" class="btn btn-block btn-primary" style="width:200px;height:200px" onclick="">
							<i class="fa fa-truck fa-3x"></i><br>
		                    <p style="font-size:18px;white-space:normal">Distributor Login</p>
		                    </button>
		                    </a>
		                <div class="clearfix"> <br> </div>
                		</div>
					</div>

               </div>
               <div class="col-sm-6">
                   <div class="container col-sm-12">
	                    
                   </div>
               </div>
               </div>
           </div>
	</div>


@endsection

@section('scripts')
    <script>
    </script>
    
@endsection