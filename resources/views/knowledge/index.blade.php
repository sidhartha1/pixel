@extends('layouts.app')

@section('content')
   {{-- @if(Session::has('customer'))
        $customer=Session::get('customer');--}}

        <section class="content-header">
        <h1>
            Knowledge Bank
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('amazon')">Amazon</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('onemg')">Medicines</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('wydr')">Wydr</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('zestmoney')">ZestMoney EMI</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('ibpay')">IB Pay</button>
                    <button type="button" class="btn btn-block btn-default" onclick="toggle_visibility('hindi')">Hindi Videos</button>
                    <div class="clearfix"> <br> </div>
                </div>

                <div class="col-sm-6">
                    <div class="container">
                    <div class="row" id="amazon" style="display: none">
                        @include('knowledge.amazon')
                    </div>

                    <div class="row" id="onemg" style="display: none">
                        @include('knowledge.onemg')
                    </div>

                    <div class="row" id="wydr" style="display: none">
                        @include('knowledge.wydr')
                    </div>

                    <div class="row" id="zestmoney" style="display: none">
                        @include('knowledge.zestmoney')
                    </div>

                    <div class="row" id="ibpay" style="display: none">
                        @include('knowledge.ibpay')
                    </div>
                    <div class="row" id="hindi" style="display: none">
                        @include('knowledge.hindi')
                    </div>	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
   {{-- @endif--}}
@endsection

@section('scripts')
    <script>
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        if (id == 'amazon') {
          document.getElementById('onemg').style.display = 'none';
          document.getElementById('wydr').style.display = 'none';
          document.getElementById('zestmoney').style.display = 'none';
          document.getElementById('ibpay').style.display = 'none';
          document.getElementById('hindi').style.display = 'none';
        }else if (id == 'onemg') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('zestmoney').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('hindi').style.display = 'none';
        }else if(id == 'wydr') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('zestmoney').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('hindi').style.display = 'none';
        }else if(id == 'zestmoney') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
            document.getElementById('hindi').style.display = 'none';
        }else if(id == 'ibpay') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById ('zestmoney').style.display = 'none';
            document.getElementById('hindi').style.display = 'none';
        }else if(id == 'hindi') {
            document.getElementById('amazon').style.display = 'none';
            document.getElementById('onemg').style.display = 'none';
            document.getElementById('wydr').style.display = 'none';
            document.getElementById ('zestmoney').style.display = 'none';
            document.getElementById('ibpay').style.display = 'none';
        }
        if (e.style.display == 'block' || e.style.display=='') e.style.display = 'none';
        else e.style.display = 'block';
    }
    </script>
@endsection
