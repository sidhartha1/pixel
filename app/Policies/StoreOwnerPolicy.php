<?php

namespace App\Policies;

use App\User;
use App\Models\StoreOwner;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class StoreOwnerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the storeOwner.
     *
     * @param  \App\User  $user
     * @param  \App\StoreOwner  $storeOwner
     * @return mixed
     */
    public function view(User $user, StoreOwner $storeOwner)
    {
        //
        return $user->id === $storeOwner->id;
    }

    /**
     * Determine whether the user can create storeOwners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ( $user->role != 'admin' ){

            return abort(403, " Unauthorized Access");
        }

                return true;

    }

    /**
     * Determine whether the user can update the storeOwner.
     *
     * @param  \App\User  $user
     * @param  \App\StoreOwner  $storeOwner
     * @return mixed
     */
    public function update(User $user, StoreOwner $storeOwner)
    {
        //
         return $user->id == $storeOwner->id;
    }

    /**
     * Determine whether the user can delete the storeOwner.
     *
     * @param  \App\User  $user
     * @param  \App\StoreOwner  $storeOwner
     * @return mixed
     */
    public function delete(User $user, StoreOwner $storeOwner)
    {
        //
        return $user->id == $storeOwner->id;
    }
    public function before()
        {
            if (Auth::user()->isAdmin()) {
                return true;
            }
        }
}
