<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check())
        {
            if ($request->has('to_freshdesk'))
            {
                return redirect()->intended(RedirectsUsers::freshdesk_login_url(Auth::user()->name, Auth::user()->email));
            }
            return redirect('/home');
        }

        return $next($request);
    }
}
