<?php

namespace App\Http\Controllers;

use App\Models\Onboarding;
use App\Models\StoreOwner;
use App\Models\UserResponse;
use App\Repositories\OnboardingRepository;
use App\Repositories\StoreOwnerRepository;
use App\Repositories\UserResponseRepository;
use Illuminate\Http\Request;

class StoreOnboardingController extends Controller
{
    /**
     * @var OnboardingRepository
     */
    private $onboardingRepository;
    /**
     * @var UserResponseRepository
     */
    private $userResponseRepository;
    /**
     * @var StoreOwnerRepository
     */
    private $storeOwnerRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OnboardingRepository $onboardingRepository, UserResponseRepository $userResponseRepository, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->middleware('auth');
        $this->onboardingRepository = $onboardingRepository;
        $this->userResponseRepository = $userResponseRepository;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('storeOnboarding.index')->with('state', 10);
    }

    public function processToken(Request $request) {
        return view('storeOnboarding.token');
    }
    
    public function check(Request $request)
    {

        //dd($request);
        $input=$request->all();
        for( $i=1;$i<100;$i++){
            if($request->input('option_'.$i)){

                /*$split = explode("_", $request->option_.$i);
             return    $qns_id=array_shift($split);*/
//                return $i;
                $answer=$request->input('option_'.$i);
                $input['owner_id']=user()->id;
                $input['question_id']=$i;
                $input['user_answer']=$answer;
              $response=$this->userResponseRepository->create($input);
            }
        }

        $url=$request->video_url;
        $owner_url=$this->storeOwnerRepository->find(user()->id);
        if($owner_url->url==null){
            $owner=StoreOwner::where('id',user()->id)->update(['url'=>$url]);
        }else{
            $ownerhasurls=StoreOwner::where('id',user()->id)->get(['url']);
            $ownerurls = $ownerhasurls[0]['url'];
            $ownerurls = $ownerurls.",".$url;
            $owner=StoreOwner::where('id',user()->id)->update(['url'=>$ownerurls]);
        }

        $ownerhasurls=StoreOwner::where('id',user()->id)->get(['url']);
        $url_array=explode(",",$ownerhasurls[0]['url']);
        $videos=Onboarding::distinct()->get(['url'])->toArray();
        $j=0;
        foreach ($videos as $video){
            $videos_url[$j] = $video['url'];
            $j++;
        }
          $video_url=array_diff($videos_url,$url_array);
            if(empty($video_url)){
               $status=StoreOwner::where('id',user()->id)->update(['onboarding_status'=>'1']);
               $correct_ans=$this->onboardingRepository->all('correct_ans');
               $user_ans=$this->userResponseRepository->all('user_answer');
              // $result=array_diff($correct_ans,$user_ans);
               return redirect(route('home'));
            }
        $videos=$this->onboardingRepository->findByField('url',$video_url['1']);
        return view('store_owners.videos',compact('videos'));
    }
}
