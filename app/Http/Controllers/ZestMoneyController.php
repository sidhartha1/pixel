<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ZestMoney;
use App\Repositories\ZestMoneyRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Flash;

class ZestMoneyController extends AppBaseController
{
    /** @var  ZestMoneyRepository */
    private $zestmoneyRepository;
    private $storeOwnerRepository;

    public function __construct(ZestMoneyRepository $zestmoneyRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->zestmoneyRepository = $zestmoneyRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    public function index(){
        if (Auth::user()->role == 'admin') {
            $zestmoney_sales = $this->zestmoneyRepository->paginate(15);
        }else {
            $email = Auth::user()->email;
            $zestmoney_sales = ZestMoney::where(['email'=>$email])->paginate(10);
        }
        return view('zestmoney.sales')->with('zestmoney_sales', $zestmoney_sales);
    }

    public function create(){
        return view('zestmoney.create');
    }

    public function store(Request $request){
        $email = Auth::user()->email;

        $zestmoney = new ZestMoney;
        
        $zestmoney->customer_id = $request->get('customer_id');
        $zestmoney->customer_name = $request->get('customer_name');
        $zestmoney->product_name = $request->get('product_name');
//         $zestmoney->product_category = $request->get('product_category');
        $zestmoney->price = $request->get('price');
        $zestmoney->quantity = $request->get('quantity');
        $zestmoney->order_id = $request->get('order_id');
        $zestmoney->date = $request->get('date');
        $zestmoney->email = $email;
//         $zestmoney->paid = $request->get('paid');
        $zestmoney->total_cost = ($zestmoney->quantity * $zestmoney->price);
//         $zestmoney->balance = (($zestmoney->total_cost) - $zestmoney->paid);
        $zestmoney->save();

        return redirect(route('zestmoney.index'));
    }

    public function show(){
        //
    }

    private function isValidDate($date_str) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
            return true;
        } else {
            return false;
        }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $email = Auth::user()->email;
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $zestmoney_sales = ZestMoney::where('created_at', '>=', $utc_today)->paginate(10);
                }else{
                    $zestmoney_sales = ZestMoney::where([['created_at', '>=', $utc_today],
                            ['email', '=', $email]])->paginate(10);
                }
                return view('zestmoney.sales')
                ->with('zestmoney_sales', $zestmoney_sales);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $zestmoney_sales = ZestMoney::where([['created_at', '>=', $utc_yesterday],
                            ['created_at', '<', $utc_today]])->paginate(10);
                }else{
                    $zestmoney_sales=ZestMoney::where([['created_at', '>=', $utc_yesterday],
                            ['created_at', '<', $utc_today],
                            ['email', '=', $email]])->paginate(10);
                }
                return view('zestmoney.sales')
                ->with('zestmoney_sales', $zestmoney_sales);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $zestmoney_sales = ZestMoney::where('created_at', '>=', $utc_today->subWeek())->paginate(10);
                }else{
                    $zestmoney_sales=ZestMoney::where([['created_at', '>=', $utc_today->subWeek()],
                            ['email', '=', $email]])->paginate(10);
                }
                return view('zestmoney.sales')
                ->with('zestmoney_sales', $zestmoney_sales);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $zestmoney_sales = ZestMoney::where('created_at', '>=', $utc_today->subMonth())->paginate(10);
                }else{
                    $zestmoney_sales=ZestMoney::where([['created_at', '>=', $utc_today->subMonth()],
                            ['email', '=', $email]])->paginate(10);
                }
                return view('zestmoney.sales')
                ->with('zestmoney_sales', $zestmoney_sales);
                break;
            default:
                list($date1, $date2) = explode(":", $filter);
                if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
                    return redirect(route('zestmoney.sales'));
                }
                $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
                $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
                if(Auth::user()->role=='admin'){
                    $zestmoney_sales= ZestMoney::where([['created_at', '>=', $date1],
                                                        ['created_at', '<=', $date2]])->paginate(10);
                }else{
                    $zestmoney_sales= ZestMoney::where([['created_at', '>=', $date1],
                                                        ['created_at', '<=', $date2],
                                                        ['email', '=', $email]])->paginate(10);
                }
                return view('zestmoney.sales')
                ->with('zestmoney_sales', $zestmoney_sales);
                break;
        }
    }
}
