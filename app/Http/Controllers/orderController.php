<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateorderRequest;
use App\Http\Requests\UpdateorderRequest;
use App\Models\order;
use App\Repositories\orderRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\DB;
use Excel;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Input;
use Response;

class orderController extends AppBaseController
{
    /** @var  orderRepository */
    private $orderRepository;
    private $storeOwnerRepository;

    public function __construct(orderRepository $orderRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    /**
     * Display a listing of the order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LatestCriteria());
        $orders = $this->orderRepository->all();

        if(Auth::user()->role=='admin'){
            $orders = $this->orderRepository->paginate(15);
        }else{
            $id=Auth::user()->id;
            $storeowner=$this->storeOwnerRepository->find($id);
            $orders=order::where(['Amazon_Id'=>$storeowner->amazon_store_id])->paginate(10);
        }

        return view('orders.index')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created order in storage.
     *
     * @param CreateorderRequest $request
     *
     * @return Response
     */
    public function store(CreateorderRequest $request)
    {
        $input = $request->all();
//        dd($input);
        if ($request->file('excel_sheets')){
//            dd($request);
            $path = $request->file('excel_sheets')->getRealPath();
            $data = \Excel::load($path)->get();
//            dd($data);
            if($data->count()){
                foreach ($data as $key => $value) {
                    $arr[] = ['Amazon_Id' => $value->tag, 'item' => $value->name, 'ASIN' => $value->asin, 'date' => $value->date, 'Quantity' => $value->qty, 'Price' => $value->price, 'created_at'=>Carbon::now()->toDateTimeString()];
                }
                // dd($arr);
                if(!empty($arr)){
                    \DB::table('orders')->insert($arr);
                    //$report = $this->reportRepository->create($arr);
                    //dd('Insert Record successfully.');
                }
            }
        }

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified order in storage.
     *
     * @param  int              $id
     * @param UpdateorderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateorderRequest $request)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('orders.index'));
    }

    private function isValidDate($date_str) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
            return true;
        } else {
            return false;
        }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $orders = order::where('date', '>=', $utc_today)->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $orders=order::where([['date', '>=', $utc_today],
                                           ['Amazon_Id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('orders.index')
                ->with('orders', $orders);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $orders = order::where([['date', '>=', $utc_yesterday],
                                            ['date', '<', $utc_today]])->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $orders=order::where([['date', '>=', $utc_yesterday],
                                          ['date', '<', $utc_today],
                                          ['Amazon_Id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('orders.index')
                ->with('orders', $orders);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $orders = order::where('date', '>=', $utc_today->subWeek())->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $orders=order::where([['date', '>=', $utc_today->subWeek()],
                                           ['Amazon_Id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('orders.index')
                ->with('orders', $orders);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $orders = order::where('date', '>=', $utc_today->subMonth())->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $orders=order::where([['date', '>=', $utc_today->subMonth()],
                                           ['Amazon_Id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('orders.index')
                ->with('orders', $orders);
                break;
            default:
                list($date1, $date2) = explode(":", $filter);
                if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
                    return redirect(route('orders.index'));
                }
                $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
                $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
                if(Auth::user()->role=='admin'){
                    $orders = order::where([['date', '>=', $date1],
                                            ['date', '<', $date2]])->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $orders=order::where([['date', '>=', $date1],
                                          ['date', '<', $date2],
                                          ['Amazon_Id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('orders.index')
                ->with('orders', $orders);
                break;
        }
    }
}
