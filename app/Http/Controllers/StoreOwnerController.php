<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateStoreOwnerRequest;
use App\Http\Requests\UpdateStoreOwnerRequest;
use App\Models\StoreOwner;
use App\Repositories\CustomerRepository;
use App\Repositories\StoreOwnerRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StoreOwnerController extends AppBaseController
{
    /** @var  StoreOwnerRepository */
    private $storeOwnerRepository;
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    public function __construct(StoreOwnerRepository $storeOwnerRepo, CustomerRepository $customerRepository)
    {
        $this->storeOwnerRepository = $storeOwnerRepo;
        $this->customerRepository = $customerRepository;
//        $this->authorizeResource(StoreOwner::class);
    }

    /**
     * Display a listing of the StoreOwner.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->storeOwnerRepository->pushCriteria(new RequestCriteria($request));
        $this->storeOwnerRepository->pushCriteria(new LatestCriteria());
        $storeOwners = $this->storeOwnerRepository->paginate(15);

        return view('store_owners.index')
            ->with('storeOwners', $storeOwners);

    }

    /**
     * Show the form for creating a new StoreOwner.
     *
     * @return Response
     */
    public function create()
    {
      //$this->authorize('create');
        return view('store_owners.create');
    }

    /**
     * Store a newly created StoreOwner in storage.
     *
     * @param CreateStoreOwnerRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreOwnerRequest $request)
    {
        $input = $request->all();

       // $input[ 'password' ] = rand ( 100000, 999999 );

        $user=User::create([
            'name'=>$input['name'],
            'email'=>$input['email'],
            'password'=>bcrypt($input['password']),
        ]);
        if($user){
            $input['id'] = $user->id;
            $storeOwner = $this->storeOwnerRepository->create($input);
        }


        Flash::success('Store Owner saved successfully.');

        return redirect(route('storeOwners.index'));
    }

    /**
     * Display the specified StoreOwner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $storeOwner = $this->storeOwnerRepository->findWithoutFail($id);

        if (empty($storeOwner)) {
            Flash::error('Store Owner not found');

            return redirect(route('storeOwners.index'));
        }

        return view('store_owners.show')->with('storeOwner', $storeOwner);
    }

    /**
     * Show the form for editing the specified StoreOwner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $storeOwner = $this->storeOwnerRepository->findWithoutFail($id);

        if (empty($storeOwner)) {
            Flash::error('Store Owner not found');

            return redirect(route('storeOwners.index'));
        }

        return view('store_owners.edit')->with('storeOwner', $storeOwner);
    }

    /**
     * Update the specified StoreOwner in storage.
     *
     * @param  int              $id
     * @param UpdateStoreOwnerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreOwnerRequest $request)
    {
        $storeOwner = $this->storeOwnerRepository->findWithoutFail($id);

        if (empty($storeOwner)) {
            Flash::error('Store Owner not found');

            return redirect(route('storeOwners.index'));
        }

        $user=User::find($id);
        if($request->password==""){
            $user->update(['name'=>$request->name]);
        }else{
            $user->update([
                'name'=>$request->name,
                'password'=>$request->password,
            ]);
        }
        $storeOwner = $this->storeOwnerRepository->update($request->all(), $id);

        Flash::success('Store Owner updated successfully.');
        return redirect(route('storeOwners.index'));
    }

    /**
     * Remove the specified StoreOwner from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $storeOwner = $this->storeOwnerRepository->findWithoutFail($id);

        if (empty($storeOwner)) {
            Flash::error('Store Owner not found');

            return redirect(route('storeOwners.index'));
        }

        $this->storeOwnerRepository->delete($id);

        Flash::success('Store Owner deleted successfully.');

        return redirect(route('storeOwners.index'));
    }

    public function checkCustomer(Request $request)
    {
        $phone= $request->phone;
        $customer=$this->customerRepository->findByField('phone',$phone);
        //dd($customer->first()->id);
        if ($customer->isEmpty()){
            Session::put('phone',$phone);
            return redirect(route('customers.create'));
        }
        Session::put('c_id',$customer->first()->id);
        Session::put('c_name',$customer->first()->name);
        Session::put('c_phone',$customer->first()->phone);
        return redirect(route('sales.create'))->with('customer',$customer);
    }

    public function submitTicket(Request $request)
    {
	$store = $request->store;
	$issue = $request->issue;
    	$description = $request->description;
    	$name = Auth::user()->name;
    	$email = Auth::user()->email;
    	$subject = $store . " | " . $issue;

    	$url = 'https://ibpixel.freshdesk.com/api/v2/tickets';
    	$body = json_encode(array("description" => $description,
									"email" => $email,
									"name" => $name,
									"subject" => $subject,
									"priority" => 1,
									"status" => 2,
									"type" => $store));
#    								"custom_fields" => array("cf_store" => $store, "cf_category" => $issue)
    	
    	$headers = ["Content-Type: application/json"];
    	
    	$ch = curl_init( $url );
    	curl_setopt( $ch, CURLOPT_POST, 1);
    	curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
    	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt( $ch, CURLOPT_USERPWD, "7ACu7Qnl8iOuEjwAyz9:X"); /* sindhu_api_key:X */
    	
    	$response = curl_exec( $ch );
    	
    	curl_close($ch);
    	
    	Flash::success('Ticket submitted successfully.');
    	
    	echo "<script type='text/javascript'>alert('Ticket submitted successfully.');</script>";
    	
    	return view('home');
    }
}
