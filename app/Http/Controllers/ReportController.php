<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Models\Report;
use App\Repositories\ReportRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Excel;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReportController extends AppBaseController
{
    /** @var  ReportRepository */
    private $reportRepository;
    /**
     * @var StoreOwnerRepository
     */
    private $storeOwnerRepository;

    public function __construct(ReportRepository $reportRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->reportRepository = $reportRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    /**
     * Display a listing of the Report.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reportRepository->pushCriteria(new RequestCriteria($request));
        $this->reportRepository->pushCriteria(new LatestCriteria());
        if(Auth::user()->role=='admin'){
            $reports = DB::table(DB::raw('(select * from reports) as a'))
                            ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                            ->groupby('amazon_store_id')->paginate(15);
            
            $all_reports = null;
        }else{
            $id=Auth::user()->id;
            $storeowner=$this->storeOwnerRepository->find($id);
            $reports = DB::table(DB::raw('(select * from reports) as a'))
                            ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                            ->where(['amazon_store_id'=>$storeowner->amazon_store_id])
                            ->groupby('amazon_store_id')
                            ->paginate(10);
            $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                            ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                            ->where(['amazon_store_id'=>$storeowner->amazon_store_id])
                            ->paginate(10);
        }

        return view('reports.index')
            ->with('reports', $reports)
            ->with('all_reports', $all_reports);
    }

    /**
     * Show the form for creating a new Report.
     *
     * @return Response
     */
    public function create()
    {
        return view('reports.create');
    }

    /**
     * Store a newly created Report in storage.
     *
     * @param CreateReportRequest $request
     *
     * @return Response
     */
    public function store(CreateReportRequest $request)
    {
        $input = $request->all();

        /*if ($request->hasFile('excel_sheet')) {
            $file = Input::file('excel_sheet');
            $name = $file->getClientOriginalName();
            // $input['excel_sheet'] = $name;

        DB::delete('DELETE FROM `reports`');
        $pdo = DB::connection() -> getPdo();
        $pdo -> exec('LOAD DATA LOCAL INFILE \''+$name+'\' INTO TABLE `create_reports_table` FIELDS TERMINATED BY \',\' OPTIONALLY ENCLOSED BY \'"\' LINES TERMINATED BY \'\\r\\n\' IGNORE 2 ROWS (amazon_store_id, @dummy, @dummy, @dummy, @dummy, ad_fees, @dummy, @dummy, newcustomerbonus, @dummy);');
        }*/

        $result = null;
        if ($request->hasFile('excel_sheet')){

            $path = $request->file('excel_sheet')->getRealPath();
            $data = \Excel::load($path, function($reader) {})->get();
//            dd($data);
            if($data->count()){
                foreach ($data as $key => $value) {
                    list($date, $time) = explode(" ", $value->date_shipped);
                    $arr[] = ['amazon_store_id' => $value->tracking_id,
                                'category' => $value->category,
                                'product_name' => $value->name,
                                'items_shipped' => $value->items_shipped,
                                'date_shipped' => $date,
                                'ad_fees' => $value->ad_fees,   
                                'newcustomerbonus' => $value->new_customer_bonus,
                                'created_at'=>Carbon::now()->toDateTimeString()];
                }
               // dd($arr);
                if(!empty($arr)){
                    \DB::table('reports')->insert($arr);
                    //$report = $this->reportRepository->create($arr);
                    //dd('Insert Record successfully.');
                }
            }
        }
        // $report = $this->reportRepository->create($input);

        Flash::success('Report saved successfully.' . $value);

        return redirect(route('reports.index'));
    }

    /**
     * Display the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error('Report not found');

            return redirect(route('reports.index'));
        }

        return view('reports.show')->with('report', $report);
    }

    /**
     * Show the form for editing the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error('Report not found');

            return redirect(route('reports.index'));
        }

        return view('reports.edit')->with('report', $report);
    }

    /**
     * Update the specified Report in storage.
     *
     * @param  int              $id
     * @param UpdateReportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportRequest $request)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error('Report not found');

            return redirect(route('reports.index'));
        }

        $report = $this->reportRepository->update($request->all(), $id);

        Flash::success('Report updated successfully.');

        return redirect(route('reports.index'));
    }

    /**
     * Remove the specified Report from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error('Report not found');

            return redirect(route('reports.index'));
        }

        $this->reportRepository->delete($id);

        Flash::success('Report deleted successfully.');

        return redirect(route('reports.index'));
    }
    public function downloadExcelFile($type){
        if(Auth::user()->role=='admin'){
            $reports = Report::get()->toArray();
        }else{
            $id=Auth::user()->id;
            $storeowner=$this->storeOwnerRepository->find($id);
            $reports=Report::where(['amazon_store_id'=>$storeowner->amazon_store_id])->get()->toArray();
        }

        return \Excel::create('Reports', function($excel) use ($reports) {
            $excel->sheet('sheet name', function($sheet) use ($reports)
            {
                $sheet->fromArray($reports);
            });
        })->download($type);

    }

    private function isValidDate($date_str) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
            return true;
        } else {
            return false;
        }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                ->where([['date_shipped', '>=', $utc_today]])
                                ->groupby('amazon_store_id')
                                ->paginate(10);
                    $all_reports = null;
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                ->where([['date_shipped', '>=', $utc_today],
                                        ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                ->groupby('amazon_store_id')
                                ->paginate(10);
                    $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                                ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                                ->where([['date_shipped', '>=', $utc_today],
                                        ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                ->paginate(10);
                }
                return view('reports.index')
                ->with('reports', $reports)
                ->with('all_reports', $all_reports);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_yesterday],
                                            ['date_shipped', '<', $utc_today]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = null;
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_yesterday],
                                            ['date_shipped', '<', $utc_today],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_yesterday],
                                            ['date_shipped', '<', $utc_today],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->paginate(10);
                }
                return view('reports.index')
                ->with('reports', $reports)
                ->with('all_reports', $all_reports);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subWeek()]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = null;
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subWeek()],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subWeek()],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->paginate(10);
                }
                return view('reports.index')
                ->with('reports', $reports)
                ->with('all_reports', $all_reports);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subMonth()]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = null;
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subMonth()],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $utc_today->subMonth()],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->paginate(10);
                }
                return view('reports.index')
                ->with('reports', $reports)
                ->with('all_reports', $all_reports);
                break;
            default:
                list($date1, $date2) = explode(":", $filter);
                if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
                    return redirect(route('reports.index'));
                }
                $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
                $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
                if(Auth::user()->role=='admin'){
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $date1],
                                            ['date_shipped', '<', $date2]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = null;
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.amazon_store_id, a.date_shipped, sum(a.ad_fees) as ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $date1],
                                             ['date_shipped', '<', $date2],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->groupby('amazon_store_id')
                                    ->paginate(10);
                    $all_reports = DB::table(DB::raw('(select * from reports) as a'))
                                    ->select(DB::raw('a.id, a.category, a.product_name, a.items_shipped, a.amazon_store_id, a.date_shipped, a.ad_fees, a.newcustomerbonus'))
                                    ->where([['date_shipped', '>=', $date1],
                                            ['date_shipped', '<', $date2],
                                            ['amazon_store_id', '=', $storeowner->amazon_store_id]])
                                    ->paginate(10);
                }
                return view('reports.index')
                ->with('reports', $reports)
                ->with('all_reports', $all_reports);
                break;
        }
    }

   /* public function lastRecords()
    {
      return  $test = Report::where('created_at', '>=', Carbon::now()->subMonth())->get();
    }*/

}


