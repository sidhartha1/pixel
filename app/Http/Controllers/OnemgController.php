<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Onemg;
use App\Repositories\OnemgRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Flash;

class OnemgController extends AppBaseController
{
    /** @var  OnemgRepository */
    private $onemgRepository;
    private $storeOwnerRepository;

    public function __construct(OnemgRepository $onemgRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->onemgRepository = $onemgRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    public function index()
    {
        if (Auth::user()->role == 'admin') {
            $onemg_sales = $this->onemgRepository->paginate(15);
        }else {
            $email = Auth::user()->email;
            $onemg_sales = Onemg::where(['email'=>$email])->paginate(10);
        }
        return view('onemg.sales')->with('onemg_sales', $onemg_sales);
    }

    public function create()
    {
        return view('onemg.create');
    }

    public function store(Request $request)
    {
        $email = Auth::user()->email;

        $onemgs = new Onemg;
        $onemgs->customer_id = $request->get('customer_id');
        $onemgs->customer_name = $request->get('customer_name');
        $onemgs->order_value = $request->get('order_value');
        $onemgs->order_id = $request->get('order_id');
        $onemgs->paid = $request->get('paid');
        $onemgs->balance = ($onemgs->order_value - $onemgs->paid);
        $onemgs->email = $email;

        $onemgs->save();

        return redirect(route('onemg.index'));
    }

    public function show(){
        //
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $email = Auth::user()->email;
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $onemg_sales = Onemg::where('created_at', '>=', $utc_today)->paginate(10);
                }else{
                    $onemg_sales = Onemg::where([['created_at', '>=', $utc_today],
                                                 ['email', '=', $email]])->paginate(10);
                }
                return view('onemg.sales')
                ->with('onemg_sales', $onemg_sales);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $onemg_sales = Onemg::where([['created_at', '>=', $utc_yesterday],
                                                 ['created_at', '<', $utc_today]])->paginate(10);
                }else{
                    $onemg_sales=Onemg::where([['created_at', '>=', $utc_yesterday],
                                               ['created_at', '<', $utc_today],
                                               ['email', '=', $email]])->paginate(10);
                }
                return view('onemg.sales')
                ->with('onemg_sales', $onemg_sales);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $onemg_sales = Onemg::where('created_at', '>=', $utc_today->subWeek())->paginate(10);
                }else{
                    $onemg_sales=Onemg::where([['created_at', '>=', $utc_today->subWeek()],
                                               ['email', '=', $email]])->paginate(10);
                }
                return view('onemg.sales')
                ->with('onemg_sales', $onemg_sales);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $onemg_sales = Onemg::where('created_at', '>=', $utc_today->subMonth())->paginate(10);
                }else{
                    $onemg_sales=Onemg::where([['created_at', '>=', $utc_today->subMonth()],
                                               ['email', '=', $email]])->paginate(10);
                }
                return view('onemg.sales')
                ->with('onemg_sales', $onemg_sales);
                break;
        }
    }
}
