<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSaleRequest;
use App\Http\Requests\UpdateSaleRequest;
use App\Models\Sale;
use App\Repositories\SaleRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SaleController extends AppBaseController
{
    /** @var  SaleRepository */
    private $saleRepository;
    private $storeOwnerRepository;

    public function __construct(SaleRepository $saleRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->saleRepository = $saleRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    /**
     * Display a listing of the Sale.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->saleRepository->pushCriteria(new RequestCriteria($request));
        if (Auth::user()-> role == 'admin') {
          $sales = $this->saleRepository->paginate(15);
        }else {
          $id = Auth::user()->id;
          $storeowner = $this->storeOwnerRepository->find($id);
          $sales = Sale::where(['amazon_store_id'=>$storeowner->amazon_store_id])->paginate(10);
        }
        return view('sales.index')
            ->with('sales', $sales);
    }

    /**
     * Show the form for creating a new Sale.
     *
     * @return Response
     */
    public function create()
    {
        return view('sales.create');
    }

    /**
     * Store a newly created Sale in storage.
     *
     * @param CreateSaleRequest $request
     *
     * @return Response
     */
    public function store(CreateSaleRequest $request)
    {
        $input = $request->all();
        $store_owner = $this->storeOwnerRepository->findWithoutFail(Auth::user()->id);
      //  dd($store_owner);
        $input['amazon_store_id'] = $store_owner->amazon_store_id;

        
        
        $sale = $this->saleRepository->create($input);

        Flash::success('Sale saved successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Display the specified Sale.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sale = $this->saleRepository->findWithoutFail($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        return view('sales.show')->with('sale', $sale);
    }

    /**
     * Show the form for editing the specified Sale.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sale = $this->saleRepository->findWithoutFail($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        return view('sales.edit')->with('sale', $sale);
    }

    /**
     * Update the specified Sale in storage.
     *
     * @param  int              $id
     * @param UpdateSaleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaleRequest $request)
    {
        $sale = $this->saleRepository->findWithoutFail($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $sale = $this->saleRepository->update($request->all(), $id);

        Flash::success('Sale updated successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Remove the specified Sale from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sale = $this->saleRepository->findWithoutFail($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $this->saleRepository->delete($id);

        Flash::success('Sale deleted successfully.');

        return redirect(route('sales.index'));
    }

    private function isValidDate($date_str) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
            return true;
        } else {
            return false;
        }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $reports = Sale::where('created_at', '>=', $utc_today)->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports=Sale::where([['created_at', '>=', $utc_today],
                                          ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('sales.index')
                    ->with('sales', $reports);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $reports = Sale::where([['created_at', '>=', $utc_yesterday],
                                            ['created_at', '<', $utc_today]])->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports=Sale::where([['created_at', '>=', $utc_yesterday],
                                          ['created_at', '<', $utc_today],
                                          ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('sales.index')
                    ->with('sales', $reports);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $reports = Sale::where('created_at', '>=', $utc_today->subWeek())->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports=Sale::where([['created_at', '>=', $utc_today->subWeek()],
                                          ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('sales.index')
                    ->with('sales', $reports);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $reports = Sale::where('created_at', '>=', $utc_today->subMonth())->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports=Sale::where([['created_at', '>=', $utc_today->subMonth()],
                                          ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('sales.index')
                    ->with('sales', $reports);
                break;
            default:
                list($date1, $date2) = explode(":", $filter);
                if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
                    return redirect(route('sales.index'));
                }
                $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
                $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
                if(Auth::user()->role=='admin'){
                    $reports = Sale::where([['created_at', '>=', $date1],
                                            ['created_at', '<=', $date2]])->paginate(10);
                }else{
                    $id=Auth::user()->id;
                    $storeowner=$this->storeOwnerRepository->find($id);
                    $reports = Sale::where([['created_at', '>=', $date1],
                                          ['created_at', '<=', $date2],
                                          ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate(10);
                }
                return view('sales.index')
                ->with('sales', $reports);
                break;
        }
    }
}
