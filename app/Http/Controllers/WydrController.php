<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wydr;
use App\Repositories\WydrRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;

class WydrController extends AppBaseController
{
    /** @var  WydrRepository */
    private $wydrRepository;
    private $storeOwnerRepository;

    public function __construct(WydrRepository $wydrRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->wydrRepository = $wydrRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    public function index(){
        if (Auth::user()->role == 'admin') {
            $wydr_sales = $this->wydrRepository->paginate(15);
        }else {
            $email = Auth::user()->email;
            $wydr_sales = Wydr::where(['email'=>$email])->paginate(10);
        }
        return view('wydr.sales')->with('wydr_sales', $wydr_sales);
    }

    public function create(){
      return view('wydr.create');
    }

    public function store(Request $request){
      $email = Auth::user()->email;

        $wydr = new Wydr;

        $wydr->customer_id = $request->get('customer_id');
        $wydr->customer_name = $request->get('customer_name');
        $wydr->product_name = $request->get('product_name');
//         $wydr->product_category = $request->get('product_category');
        $wydr->price = $request->get('price');
        $wydr->quantity = $request->get('quantity');
        $wydr->shipping_cost = $request->get('shipping_cost');
        $wydr->order_id = $request->get('order_id');
        $wydr->date = $request->get('date');
        $wydr->email = $email;
        $wydr->paid = $request->get('paid');
        $wydr->total_cost = ($wydr->quantity * $wydr->price);
        $wydr->balance = (($wydr->total_cost + $wydr->shipping_cost) - $wydr->paid);
        $wydr->save();

        return redirect(route('wydr.index'));
    }

    public function show(){
        //
    }

    private function isValidDate($date_str) {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
            return true;
        } else {
            return false;
        }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
        $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
        $email = Auth::user()->email;
        switch($filter){
            case 'today':
                if(Auth::user()->role=='admin'){
                    $wydr_sales = Wydr::where('created_at', '>=', $utc_today)->paginate(10);
                }else{
                    $wydr_sales = Wydr::where([['created_at', '>=', $utc_today],
                                               ['email', '=', $email]])->paginate(10);
                }
                return view('wydr.sales')
                ->with('wydr_sales', $wydr_sales);
                break;
            case 'yesterday':
                if(Auth::user()->role=='admin'){
                    $wydr_sales = Wydr::where([['created_at', '>=', $utc_yesterday],
                            ['created_at', '<', $utc_today]])->paginate(10);
                }else{
                    $wydr_sales=Wydr::where([['created_at', '>=', $utc_yesterday],
                                          ['created_at', '<', $utc_today],
                                          ['email', '=', $email]])->paginate(10);
                }
                return view('wydr.sales')
                ->with('wydr_sales', $wydr_sales);
                break;
            case 'week':
                if(Auth::user()->role=='admin'){
                    $wydr_sales = Wydr::where('created_at', '>=', $utc_today->subWeek())->paginate(10);
                }else{
                    $wydr_sales=Wydr::where([['created_at', '>=', $utc_today->subWeek()],
                                          ['email', '=', $email]])->paginate(10);
                }
                return view('wydr.sales')
                ->with('wydr_sales', $wydr_sales);
                break;
            case 'month':
                if(Auth::user()->role=='admin'){
                    $wydr_sales = Wydr::where('created_at', '>=', $utc_today->subMonth())->paginate(10);
                }else{
                    $wydr_sales=Wydr::where([['created_at', '>=', $utc_today->subMonth()],
                                          ['email', '=', $email]])->paginate(10);
                }
                return view('wydr.sales')
                ->with('wydr_sales', $wydr_sales);
                break;
            default:
                list($date1, $date2) = explode(":", $filter);
                if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
                    return redirect(route('wydr.sales'));
                }
                $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
                $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
                if(Auth::user()->role=='admin'){
                    $wydr_sales= Wydr::where([['created_at', '>=', $date1],
                                              ['created_at', '<=', $date2]])->paginate(10);
                }else{
                    $wydr_sales= Wydr::where([['created_at', '>=', $date1],
                                              ['created_at', '<=', $date2],
                                              ['email', '=', $email]])->paginate(10);
                }
                return view('wydr.sales')
                ->with('wydr_sales', $wydr_sales);
                break;
        }
    }
}
