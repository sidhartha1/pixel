<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use App\Models\Sale;
use App\NoSale;
use App\Wydr;
use App\Onemg;
use App\ZestMoney;
use App\Repositories\CustomerRepository;
use App\Repositories\StoreOwnerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;
    private $storeOwnerRepository;

    public function __construct(CustomerRepository $customerRepo, StoreOwnerRepository $storeOwnerRepo)
    {
        $this->customerRepository = $customerRepo;
        $this->storeOwnerRepository = $storeOwnerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $this->customerRepository->pushCriteria(new LatestCriteria());
        if(Auth::user()->role=='admin'){
            $customers = $this->customerRepository->paginate(15);
        }else{
            $id=Auth::user()->id;
            $customers = Customer::where(['user_id'=>$id])->latest()->paginate(10);
//             $customers=Customer::where(['user_id'=>$id])->latest()->paginate(10);
        // return   $customers =  $this->customerRepository->findWhere(['user_id'=>$id]);
        }
        return view('customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store nosales reason of a Customer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function nosales(Request $request)
    {
        Flash::success('Customer data saved successfully.');
        $email = Auth::user()->email;

        $nosale = new NoSale;
        $nosale->customer_id = $request->get('customer_id');
        $nosale->customer_name = $request->get('customer_name');
        $nosale->reason = $request->get('reason');
        $nosale->email = $email;
        $nosale->save();

        return redirect(route('customers.index'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();

         $input['user_id']=Auth::user()->id;

        $customer = $this->customerRepository->create($input);
//dd($customer);
        Flash::success('Customer saved successfully.');

        if(user()->role=="storeowner"){
           // dd($customer);
            $phone = $input['phone'];
            $customer=$this->customerRepository->findByField('phone',$phone);
            Session::put('c_id',$customer->first()->id);
            Session::put('c_name',$customer->first()->name);
            Session::put('c_phone',$customer->first()->phone);

            return redirect(route('sales.create'))->with('customer',$customer);
        }
        return redirect(route('customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }

    public function salesfilter($filter){
        $id = Auth::user()->id;
        $email = Auth::user()->email;
        $storeowner = $this->storeOwnerRepository->find($id);

        if(Auth::user()->role=='admin'){
            $amazon_sales = Sale::where('customer_id', '=', $filter);
        }else{
            $amazon_sales = Sale::where([['customer_id', '=', $filter],
                                         ['amazon_store_id', '=', $storeowner->amazon_store_id]])->paginate();
        }

        if(Auth::user()->role=='admin'){
            $wydr_sales = Wydr::where('customer_id', '=', $filter);
        }else{
            $wydr_sales = Wydr::where([['customer_id', '=', $filter],
                                       ['email', '=', $email]])->paginate();
        }

        if(Auth::user()->role=='admin'){
            $onemg_sales = Onemg::where('customer_id', '=', $filter);
        }else{
            $onemg_sales = Onemg::where([['customer_id', '=', $filter],
                                         ['email', '=', $email]])->paginate();
        }

        if(Auth::user()->role=='admin'){
            $zestmoney_sales = ZestMoney::where('customer_id', '=', $filter);
        }else{
            $zestmoney_sales = ZestMoney::where([['customer_id', '=', $filter],
                                                 ['email', '=', $email]])->paginate();
        }

        return view('customersales.index')
        ->with('amazon_sales', $amazon_sales)
        ->with('wydr_sales', $wydr_sales)
        ->with('onemg_sales', $onemg_sales)
        ->with('zestmoney_sales', $zestmoney_sales);
    }
}
