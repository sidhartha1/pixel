<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateOnboardingRequest;
use App\Http\Requests\UpdateOnboardingRequest;
use App\Repositories\OnboardingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OnboardingController extends AppBaseController
{
    /** @var  OnboardingRepository */
    private $onboardingRepository;

    public function __construct(OnboardingRepository $onboardingRepo)
    {
        $this->onboardingRepository = $onboardingRepo;
    }

    /**
     * Display a listing of the Onboarding.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->onboardingRepository->pushCriteria(new RequestCriteria($request));
        $this->onboardingRepository->pushCriteria(new LatestCriteria());
        $onboardings = $this->onboardingRepository->paginate(15);

        return view('onboardings.index')
            ->with('onboardings', $onboardings);
    }

    /**
     * Show the form for creating a new Onboarding.
     *
     * @return Response
     */
    public function create()
    {
        return view('onboardings.create');
    }

    /**
     * Store a newly created Onboarding in storage.
     *
     * @param CreateOnboardingRequest $request
     *
     * @return Response
     */
    public function store(CreateOnboardingRequest $request)
    {
        $input = $request->all();

        $onboarding = $this->onboardingRepository->create($input);

        Flash::success('Onboarding saved successfully.');

        return redirect(route('onboardings.index'));
    }

    /**
     * Display the specified Onboarding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $onboarding = $this->onboardingRepository->findWithoutFail($id);

        if (empty($onboarding)) {
            Flash::error('Onboarding not found');

            return redirect(route('onboardings.index'));
        }

        return view('onboardings.show')->with('onboarding', $onboarding);
    }

    /**
     * Show the form for editing the specified Onboarding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $onboarding = $this->onboardingRepository->findWithoutFail($id);

        if (empty($onboarding)) {
            Flash::error('Onboarding not found');

            return redirect(route('onboardings.index'));
        }

        return view('onboardings.edit')->with('onboarding', $onboarding);
    }

    /**
     * Update the specified Onboarding in storage.
     *
     * @param  int              $id
     * @param UpdateOnboardingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOnboardingRequest $request)
    {
        $onboarding = $this->onboardingRepository->findWithoutFail($id);

        if (empty($onboarding)) {
            Flash::error('Onboarding not found');

            return redirect(route('onboardings.index'));
        }

        $onboarding = $this->onboardingRepository->update($request->all(), $id);

        Flash::success('Onboarding updated successfully.');

        return redirect(route('onboardings.index'));
    }

    /**
     * Remove the specified Onboarding from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $onboarding = $this->onboardingRepository->findWithoutFail($id);

        if (empty($onboarding)) {
            Flash::error('Onboarding not found');

            return redirect(route('onboardings.index'));
        }

        $this->onboardingRepository->delete($id);

        Flash::success('Onboarding deleted successfully.');

        return redirect(route('onboardings.index'));
    }
}
