<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Loan;
use App\Repositories\LoanRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CustomerRepository;
use App\Repositories\StoreOwnerRepository;
use Carbon\Carbon;
use DateTimeZone;
use Flash;

class LoanyantraController extends AppBaseController
{
    /** @var  LoanRepository */
    private $loanRepository;
    private $customerRepository;
    private $storeOwnerRepository;

    public function __construct(LoanRepository $loanRepository, CustomerRepository $customerRepository, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->loanRepository = $loanRepository;
        $this->customerRepository = $customerRepository;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    public function getCompanyList() {
        $url = "http://API.LOANYATRA.COM/API/V1/GetCompanyList";
//         $headers = ["Content-Type: application/json"];
//         $body = json_encode(array(
//                 "client_id" => "8297727911320749",
//                 "client_secret" => "7a3d3e44693d3afe86c8d2ede5948cd39a7af058dbb97868",
//                 "grant_type" => "password",
//                 "username" => "sindhu@kiwistalabs.com",
//                 "password" => "qweQWE1!"));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_HTTPGET, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
//         curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));
        curl_close($ch);

        return $response;
    }

    public function index(){
        return view('loanyantra.index');
//         if (Auth::user()->role == 'admin') {
//             $wydr_sales = $this->wydrRepository->paginate(15);
//         }else {
//             $email = Auth::user()->email;
//             $wydr_sales = Wydr::where(['email'=>$email])->paginate(10);
//         }
//         return view('wydr.sales')->with('wydr_sales', $wydr_sales);
    }

    public function create(){
        return view('loanyantra.create');
    }

    public function storeCustomer(Request $request) {
        $input = $request->all();

        $input['user_id']=Auth::user()->id;

        $customer = $this->customerRepository->create($input);
        Flash::success('Customer saved successfully.');

        if(user()->role=="storeowner"){
            return $this->checkEligibility($request);
        }
        return redirect(route('customers.index'));
    }

    public function store(Request $request){
        $email = Auth::user()->email;

        $customer=$this->customerRepository->findByField('phone',$request->get('phone'));
        if ($customer->isEmpty()){
            $input = $request->all();
            $input['user_id'] = Auth::user()->id;
            $customer = $this->customerRepository->create($input);
        }

        $loan = new Loan;
        $loan->customer_name = $request->get('name');
        $loan->customer_mobile = $request->get('phone');
        $loan->customer_email = $request->get('email');
        $loan->customer_age = $request->get('age');
        $loan->customer_gender = $request->get('gender');
        $loan->gross_monthly_salary = $request->get('gross_month_salary');
        $loan->exisiting_emis = $request->get('existing_emis');
        $loan->property_location = $request->get('property_location');
        $loan->working_location = $request->get('working_location');
        $loan->type_of_loan = $request->get('loan_type');
        $loan->company_name = $request->get('company_name');
        $loan->date = $request->get('date');
        $loan->store_email = $email;

        $url = "http://API.LOANYATRA.COM/API/V1/generationLead";
        $headers = ["Content-Type: application/json"];
        $body = json_encode(array(
                "customerName"=> $request->get('name'),
                "Age"=> $request->get('age'),
                "propertyLocation"=> $request->get('property_location'),
                "companyName"=> $request->get('company_name'),
                "LoanType"=> $request->get('loan_type'),
                "emailId"=> $request->get('email'),
                "mobileNumber"=> $request->get('phone'),
                "workingLocation"=> $request->get('working_location'),
                "password"=> "indiaBuys@LY",
                "ExistingLoanEMI"=> $request->get('existing_emis'),
                "vendorUUID"=> "008BE9CE-10D1-40FF-indiaBuys-C48A0962LYPR",
                "CampId"=> $email,
                "accountStatus"=> "ACCOUNT_CREATED",
                "MonthlySalary"=> $request->get('gross_month_salary')
        ));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(json_decode(curl_exec( $ch )), true);
        $results = $response['EligibilityData'];
        curl_close($ch);

        if (empty($results)) {
            Flash::warning('Sorry. As per the given information our partners cannot provide a loan for you. Thank you.');
            $loan->eligibility = 0;
        } else {
            Flash::success('Congratulations!! You are eligible to receive a Loan! Please find the ' .
                           'quotes from our partners below. You will shortly receive a call from our team.');
            $loan->eligibility = 1;
        }
        $loan->save();

        return view('loanyantra.results')->with('results', $results);
//         $email = Auth::user()->email;

//         $wydr = new Wydr;

//         $wydr->customer_id = $request->get('customer_id');
//         $wydr->customer_name = $request->get('customer_name');
//         $wydr->product_name = $request->get('product_name');
//         //         $wydr->product_category = $request->get('product_category');
//         $wydr->price = $request->get('price');
//         $wydr->quantity = $request->get('quantity');
//         $wydr->shipping_cost = $request->get('shipping_cost');
//         $wydr->order_id = $request->get('order_id');
//         $wydr->date = $request->get('date');
//         $wydr->email = $email;
//         $wydr->paid = $request->get('paid');
//         $wydr->total_cost = ($wydr->quantity * $wydr->price);
//         $wydr->balance = (($wydr->total_cost + $wydr->shipping_cost) - $wydr->paid);
//         $wydr->save();

//         return redirect(route('wydr.index'));
    }

    public function show(){
        //
    }

    public function checkCustomerLoans(Request $request)
    {
        switch($request->submitbutton) {
            case 'Apply for a Loan':
                return $this->checkEligibility($request);
                break;
            case 'View Previous Loans':
                return $this->displayLoans($request);
                break;
        }
    }


    public function checkEligibility(Request $request)
    {
        $phone= $request->phone;
        $customer=$this->customerRepository->findByField('phone',$phone);
        //dd($customer->first()->id);
        if ($customer->isEmpty()){
            Session::forget(['c_id', 'c_name', 'c_phone', 'c_email', 'c_age', 'c_gender', 'c_profession', 'c_source']);
            Session::put('c_phone',$phone);
            return view('loanyantra.customer');
        }
        Session::put('c_id',$customer->first()->id);
        Session::put('c_name',$customer->first()->name);
        Session::put('c_phone',$customer->first()->phone);
        Session::put('c_email',$customer->first()->email);
        Session::put('c_age',$customer->first()->age);
        Session::put('c_gender',$customer->first()->gender);
        Session::put('c_profession',$customer->first()->profession);
        Session::put('c_source',$customer->first()->source);
        return redirect(route('loanyantra.create'))->with('customer',$customer);
    }

    public function displayLoans(Request $request){
//         $url = "http://api.loanyatra.com/API/V1/GetLeadsByPatner?vendorUUID=008BE9CE-10D1-40FF-indiaBuys-C48A0962LYPR&&Partner_ID=123";

//         $ch = curl_init( $url );
//         curl_setopt( $ch, CURLOPT_HTTPGET, 1);
//         curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
//         curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

//         $response = json_decode(json_decode(curl_exec( $ch )), true);
//         $results = $response;
//         curl_close($ch);

//         return view('loanyantra.loans')->with('results', $results);
//         print_r($request);
        if (Auth::user()->role == 'admin') {
            $laons = $this->loanRepository->paginate(15);
        }else {
            $email = Auth::user()->email;
            $mobile = $request->phone;
            $laons = Loan::where(['store_email'=>$email, 'customer_mobile'=>$mobile])->paginate(10);
        }
        return view('loanyantra.loans')->with('loans', $laons);
    }

    private function isValidDate($date_str) {
//         if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_str)) {
//             return true;
//         } else {
//             return false;
//         }
    }

    public function filter($filter){
        // This is needed to display as per IST and query as per UTC
//         $utc_today = Carbon::today(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
//         $utc_yesterday = Carbon::yesterday(new DateTimeZone('Asia/Kolkata'))->setTimezone(new DateTimeZone("UTC"));
//         $email = Auth::user()->email;
//         switch($filter){
//             case 'today':
//                 if(Auth::user()->role=='admin'){
//                     $wydr_sales = Wydr::where('created_at', '>=', $utc_today)->paginate(10);
//                 }else{
//                     $wydr_sales = Wydr::where([['created_at', '>=', $utc_today],
//                             ['email', '=', $email]])->paginate(10);
//                 }
//                 return view('wydr.sales')
//                 ->with('wydr_sales', $wydr_sales);
//                 break;
//             case 'yesterday':
//                 if(Auth::user()->role=='admin'){
//                     $wydr_sales = Wydr::where([['created_at', '>=', $utc_yesterday],
//                             ['created_at', '<', $utc_today]])->paginate(10);
//                 }else{
//                     $wydr_sales=Wydr::where([['created_at', '>=', $utc_yesterday],
//                             ['created_at', '<', $utc_today],
//                             ['email', '=', $email]])->paginate(10);
//                 }
//                 return view('wydr.sales')
//                 ->with('wydr_sales', $wydr_sales);
//                 break;
//             case 'week':
//                 if(Auth::user()->role=='admin'){
//                     $wydr_sales = Wydr::where('created_at', '>=', $utc_today->subWeek())->paginate(10);
//                 }else{
//                     $wydr_sales=Wydr::where([['created_at', '>=', $utc_today->subWeek()],
//                             ['email', '=', $email]])->paginate(10);
//                 }
//                 return view('wydr.sales')
//                 ->with('wydr_sales', $wydr_sales);
//                 break;
//             case 'month':
//                 if(Auth::user()->role=='admin'){
//                     $wydr_sales = Wydr::where('created_at', '>=', $utc_today->subMonth())->paginate(10);
//                 }else{
//                     $wydr_sales=Wydr::where([['created_at', '>=', $utc_today->subMonth()],
//                             ['email', '=', $email]])->paginate(10);
//                 }
//                 return view('wydr.sales')
//                 ->with('wydr_sales', $wydr_sales);
//                 break;
//             default:
//                 list($date1, $date2) = explode(":", $filter);
//                 if (!$this->isValidDate($date1) || !$this->isValidDate($date2)) {
//                     return redirect(route('wydr.sales'));
//                 }
//                 $date1 = Carbon::createFromFormat("Y-m-d H", trim($date1) . " 0");
//                 $date2 = Carbon::createFromFormat("Y-m-d H", trim($date2) . " 24");
//                 if(Auth::user()->role=='admin'){
//                     $wydr_sales= Wydr::where([['created_at', '>=', $date1],
//                             ['created_at', '<=', $date2]])->paginate(10);
//                 }else{
//                     $wydr_sales= Wydr::where([['created_at', '>=', $date1],
//                             ['created_at', '<=', $date2],
//                             ['email', '=', $email]])->paginate(10);
//                 }
//                 return view('wydr.sales')
//                 ->with('wydr_sales', $wydr_sales);
//                 break;
//         }
    }
}
