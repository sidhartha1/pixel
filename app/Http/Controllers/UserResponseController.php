<?php

namespace App\Http\Controllers;

use App\Criteria\LatestCriteria;
use App\Http\Requests\CreateUserResponseRequest;
use App\Http\Requests\UpdateUserResponseRequest;
use App\Repositories\UserResponseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserResponseController extends AppBaseController
{
    /** @var  UserResponseRepository */
    private $userResponseRepository;

    public function __construct(UserResponseRepository $userResponseRepo)
    {
        $this->userResponseRepository = $userResponseRepo;
    }

    /**
     * Display a listing of the UserResponse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userResponseRepository->pushCriteria(new RequestCriteria($request));
        $this->userResponseRepository->pushCriteria(new LatestCriteria());
        $userResponses = $this->userResponseRepository->paginate(15);

        return view('user_responses.index')
            ->with('userResponses', $userResponses);
    }

    /**
     * Show the form for creating a new UserResponse.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_responses.create');
    }

    /**
     * Store a newly created UserResponse in storage.
     *
     * @param CreateUserResponseRequest $request
     *
     * @return Response
     */
    public function store(CreateUserResponseRequest $request)
    {
        $input = $request->all();

        $userResponse = $this->userResponseRepository->create($input);

        Flash::success('User Response saved successfully.');

        return redirect(route('userResponses.index'));
    }

    /**
     * Display the specified UserResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userResponse = $this->userResponseRepository->findWithoutFail($id);

        if (empty($userResponse)) {
            Flash::error('User Response not found');

            return redirect(route('userResponses.index'));
        }

        return view('user_responses.show')->with('userResponse', $userResponse);
    }

    /**
     * Show the form for editing the specified UserResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userResponse = $this->userResponseRepository->findWithoutFail($id);

        if (empty($userResponse)) {
            Flash::error('User Response not found');

            return redirect(route('userResponses.index'));
        }

        return view('user_responses.edit')->with('userResponse', $userResponse);
    }

    /**
     * Update the specified UserResponse in storage.
     *
     * @param  int              $id
     * @param UpdateUserResponseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserResponseRequest $request)
    {
        $userResponse = $this->userResponseRepository->findWithoutFail($id);

        if (empty($userResponse)) {
            Flash::error('User Response not found');

            return redirect(route('userResponses.index'));
        }

        $userResponse = $this->userResponseRepository->update($request->all(), $id);

        Flash::success('User Response updated successfully.');

        return redirect(route('userResponses.index'));
    }

    /**
     * Remove the specified UserResponse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userResponse = $this->userResponseRepository->findWithoutFail($id);

        if (empty($userResponse)) {
            Flash::error('User Response not found');

            return redirect(route('userResponses.index'));
        }

        $this->userResponseRepository->delete($id);

        Flash::success('User Response deleted successfully.');

        return redirect(route('userResponses.index'));
    }
}
