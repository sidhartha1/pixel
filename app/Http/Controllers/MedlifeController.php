<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use App\MedlifeOrder;
use App\MedlifeImageIndex;
use App\Repositories\MedlifeOrderRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\StoreOwnerRepository;
// use Carbon\Carbon;
// use DateTimeZone;

class MedlifeController extends AppBaseController
{
    /** @var  MedlifeOrderRepository */
    private $medlifeOrderRepository;
    private $storeOwnerRepository;

    public function __construct(MedlifeOrderRepository $medlifeOrderRepo, StoreOwnerRepository $storeOwnerRepository)
    {
        $this->medlifeOrderRepository = $medlifeOrderRepo;
        $this->storeOwnerRepository = $storeOwnerRepository;
    }

    private $medlifeRefreshToken;

    public function index(){
        if (Auth::user()->role == 'admin') {
            $medlife_orders = $this->medlifeOrderRepository->paginate(15);
        }else {
            $email = Auth::user()->email;
            $medlife_orders = MedlifeOrder::where(['email'=>$email])->paginate(10);
        }
        return view('medlife.sales')->with('medlife_orders', $medlife_orders);
    }

    public function create(){
        return view('medlife.create');
    }

    private function getMedlifeRefreshToken() {
        $url = "https://svc.medlife.com/ml-rest-services/api/v1/login";
        $headers = ["Content-Type: application/json"];
        $body = json_encode(array(
                "client_id" => "6747109899262802",
                "client_secret" => "2b2ce1e8fd2a902619c154761078cdba99c6d63bf342e14d",
                "grant_type" => "password",
                "username" => "sindhu@kiwistalabs.com",
                "password" => "qweQWE1!"));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));
        curl_close($ch);

        $this->medlifeRefreshToken = $response->refresh_token;
        return $this->medlifeRefreshToken;
    }

    private function getMedlifeAccessToken($refresh_token) {
        $url = "https://svc.medlife.com/ml-rest-services/api/v1/token";
        $headers = ["Content-Type: application/json"];
        $body = json_encode(array(
                "grant_type" => "refresh_token",
                "refresh_token" => $refresh_token,
                "client_id" => "6747109899262802",
                "client_secret" => "2b2ce1e8fd2a902619c154761078cdba99c6d63bf342e14d"));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));
        curl_close($ch);

        if (isset($response->errorCode)) {
            print_r($response);
            return null;
        }

        return $response->access_token ;
    }

    private function getMedlifeDraftRX(Request $request, $access_token) {
        $url = "https://svc.medlife.com/ml-rest-services/api/v1/rx";
        $images = $request->file('upload_image');
        $uploadCount = sizeof($images);
        $headers = ["Content-Type: application/json",
                    "Authorization: Bearer " . $access_token];
        $body = json_encode(array(
                "customerId" => $request->get('customer_id'),
                "firstName" => $request->get('customer_name'),
                "mobile" => $request->get('customer_phone'),
                "uploadCount" => $uploadCount,
                "fbm" => true,
                "deliveryAddress" => array("name" => $request->get('delivery_name'),
                                           "pinCode" => $request->get('delivery_pincode'),
                                           "city" => $request->get('delivery_city'),
                                           "contactNumber" => $request->get('delivery_mobile'),
                                           "addressLine1" => $request->get('delivery_add_1'),
                                           "addressLine2" => $request->get('delivery_add_2')),
                "discounts" => array(
                        array("name" => "Test",
                              "value" => 15,
                              "type" => "PERCENTAGE")
                )));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));
        curl_close($ch);

        if (isset($response->errorCode)) {
            print_r($response);
            return null;
        }

        return $response->rxId;
    }

    private function sendMedlifeDraftRX($access_token, $draftRxId, $image_base64, $imageIndex, $description) {
        $url = "https://svc.medlife.com/ml-rest-services/api/v1/rx/image";
        $headers = ["Content-Type: application/json",
                "Authorization: Bearer " . $access_token];
        $body = json_encode(array(
                "rxId" => $draftRxId,
                "description" => $description,
                "image" => $image_base64,
                "imageIndex" => $imageIndex));

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));
        curl_close($ch);

        if (isset($response->errorCode)) {
            print_r($response);
            return null;
        }

        return $response;
    }

    private function uploadMedlifeImageRX($access_token, $draftRxId, $image, $imageIndex, $description) {
        $extension = $image->extension();
        $path = $image->path();
        $data = file_get_contents($path);
        $image_base64 = "data:image/" . $extension . ";base64," . base64_encode($data);
        $response = $this->sendMedlifeDraftRX($access_token, $draftRxId, $image_base64, $imageIndex, $description);
        if ($response == null) {
            Flash::error("Send Draft Rx failed for image-." . $imageIndex . " failed.");
            return view('medlife.create');
        }
        return $response;
    }

    public function store(Request $request){
        $access_token = null;
        if($this->medlifeRefreshToken) {
            $access_token = $this->getMedlifeAccessToken($this->medlifeRefreshToken);
        }

        if($access_token == null) {
            $refresh_token = $this->getMedlifeRefreshToken();
            $access_token = $this->getMedlifeAccessToken($refresh_token);
        }

        if ($access_token == null) {
            Flash::error("Unable to login.");
            return view('medlife.create');
        }

        $draftRxId = $this->getMedlifeDraftRX($request, $access_token);
        $images = $request->file('upload_image');
        $imageIndex = 0;
        $uploadCount = sizeof($images);

        if ($draftRxId) {
            $email = Auth::user()->email;
            $medlifeOrder = new MedlifeOrder;
            $medlifeOrder->email = $email;
            $medlifeOrder->rx_id = $draftRxId;
            $medlifeOrder->customer_id = $request->get('customer_id');
            $medlifeOrder->customer_name = $request->get('customer_name');
            $medlifeOrder->upload_count = $uploadCount;
            $medlifeOrder->delivery_name = $request->get('delivery_name');
            $medlifeOrder->delivery_mobile = $request->get('delivery_mobile');
            $medlifeOrder->delivery_add_1 = $request->get('delivery_add_1');
            $medlifeOrder->delivery_add_2 = $request->get('delivery_add_2');
            $medlifeOrder->delivery_city = $request->get('delivery_city');
            $medlifeOrder->delivery_pincode = $request->get('delivery_pincode');
            $medlifeOrder->date = $request->get('date');
            $medlifeOrder->save();

            foreach($images as $image) {
                $response = $this->uploadMedlifeImageRX($access_token, $draftRxId, $image, $imageIndex, "Indiabuys order from " . $request->get("customer_name"));
                $medlifeImageIndex = new MedlifeImageIndex;
                $medlifeImageIndex->rx_id = $response->rxId;
                $medlifeImageIndex->image_index = $response->imageIndex;
                $medlifeImageIndex->image_id = $response->imageId;
                $medlifeImageIndex->save();
                $imageIndex = $imageIndex + 1;
            }
        } else {
            Flash::error("Get Draft Rx failed.");
            return view('medlife.create');
        }

        Flash::success("Order placed! RxId - " . $response->rxId);
        return redirect(route('medlife.index'));
    }

    private function getOrderFields($orderId) {
        $access_token = null;
        if($this->medlifeRefreshToken) {
            $access_token = $this->getMedlifeAccessToken($this->medlifeRefreshToken);
        }

        if($access_token == null) {
            $refresh_token = $this->getMedlifeRefreshToken();
            $access_token = $this->getMedlifeAccessToken($refresh_token);
        }

        if ($access_token == null) {
            Flash::error("Unable to login.");
            return null;
        }

        $url = "https://svc.medlife.com/ml-rest-services/api/v1/order/" . $orderId;
        $headers = ["Content-Type: application/json",
                    "Authorization: Bearer " . $access_token];

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_HTTPGET, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );
        curl_close($ch);

        return $response;
    }

    public function updateOrder(Request $request) {
        $orderId = $request->get('id');
        $response = $this->getOrderFields($orderId);
        $arr_response = json_decode($response);

        if (!$arr_response) {
            return json_encode(array(
                    "status" => False,
                    "message" => "Unable to get order."));
        }

        $rxId = $arr_response->rxId;
        $status = $arr_response->status;
        $estimatedDeliveryDate = $arr_response->estimatedDeliveryDate;
        $totalAmount = $arr_response->totalAmount;
        $payableAmount = $arr_response->payableAmount;
        $discountAmount = $arr_response->discountAmount;
        $createdTime = $arr_response->createdTime;
        $deliveryCharge = $arr_response->deliveryCharge;
        $items = $arr_response->items;
        $customerId = $arr_response->customerId;

        try{
            $values=array('order_id'=>$orderId,
                          'estimated_delivery_date'=>$estimatedDeliveryDate,
                          'total_amount'=>$totalAmount,
                          'payable_amount'=>$payableAmount,
                          'discount_amount'=>$discountAmount,
                          'order_state' => $status);
            MedlifeOrder::where(['rx_id'=>$rxId])->update($values);
        }
        catch(\Exception $e) {
            $ret = json_encode(array(
                    "status" => False,
                    "message" => $e->getMessage()));
            return $ret;
        }

        $ret = json_encode(array(
                "status" => True,
                "rxId" => $rxId,
                "orderId" => $orderId,
                "orderStatus" => $status,
                "message" => "Order updated successfully"));
        return $ret;
    }

    public function show(){
        //
    }

    public function filter($filter){
        //
    }
}
