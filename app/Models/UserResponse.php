<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserResponse
 * @package App\Models
 * @version August 24, 2017, 10:50 am UTC
 *
 * @method static UserResponse find($id=null, $columns = array())
 * @method static UserResponse|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property unsignedInteger owner_id
 * @property unsignedInteger question_id
 * @property string user_answer
 */
class UserResponse extends Model
{
    use SoftDeletes;

    public $table = 'user_responses';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'owner_id',
        'question_id',
        'user_answer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_answer' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
