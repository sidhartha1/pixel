<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version August 15, 2017, 8:08 am UTC
 *
 * @method static Customer find($id=null, $columns = array())
 * @method static Customer|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property UnsignedInteger owner_id
 * @property string name
 * @property string email
 * @property string phone
 * @property string age
 * @property string gender
 * @property string address
 * @property string city
 * @property string state
 * @property double price
 * @property date date
 * @property time time
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'name',
        'phone',
        'email',
        'age',
        'gender',
        'profession',
        'source',
        'date',
        'time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'age' => 'string',
        'gender' => 'string',
        'profession' => 'string',
        'source' => 'string',
        'price' => 'double',
        'date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'age' => 'required',
    ];
 public function user ()
        {
            return $this->belongsTo(StoreOwner::class, 'user_id');
        }

}
