<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sale
 * @package App\Models
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method static Sale find($id=null, $columns = array())
 * @method static Sale|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property UnsignedInteger customer_id
 * @property string customer_name
 * @property string product_name
 * @property string product_id
 * @property string product_quantity
 * @property double price
 * @property boolean paid
 * @property double balance
 * @property date date
 */
class Sale extends Model
{
    use SoftDeletes;

    public $table = 'sales';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'amazon_store_id',
        'customer_id',
        'customer_name',
        'product_name',
        'product_id',
        'product_quantity',
        'price',
        'paid',
        'balance',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_name' => 'string',
        'product_id' => 'string',
        'product_quantity' => 'string',
        'price' => 'double',
        'paid' => 'double',
        'balance' => 'double',
        'date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_name' => 'required',
        'product_id' => 'required',
        'product_quantity' => 'required',
        'price' => 'required'
    ];


}
