<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

/**
 * Class StoreOwner
 * @package App\Models
 * @version August 15, 2017, 5:32 am UTC
 *
 * @method static StoreOwner find($id=null, $columns = array())
 * @method static StoreOwner|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string name
 * @property string email
 * @property string password
 * @property date dob
 * @property string gender
 * @property string phone
 * @property string source
 * @property string amazon_store_id
 * @property string Previous_profession
 * @property string pan_number
 * @property string aadhar_number
 * @property string account_num
 * @property string onboarding_status
 */
class StoreOwner extends Model
{
    use SoftDeletes;

    public $table = 'store_owners';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
    'id',

        'email',

        'dob',
        'gender',
        'phone',
        'source',
        'amazon_store_id',
        'Previous_profession',
        'pan_number',
        'aadhar_number',
        'account_num',
        'onboarding_status',
        'url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

        'email' => 'string',

        'dob' => 'date',
        'gender' => 'string',
        'phone' => 'string',
        'source' => 'string',
        'amazon_store_id' => 'string',
        'Previous_profession' => 'string',
        'pan_number' => 'string',
        'aadhar_number' => 'string',
        'account_num' => 'string',
        'onboarding_status' => 'string',
        'url'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        /*'email' => 'required',,*/
        'dob' => 'required',
       /* 'gender' => 'required',*/
        'phone' => 'required',
       /* 'source' => 'required',*/
       /* 'amazon_store_id' => 'required',
        'Previous_profession' => 'required',
        'pan_number' => 'required',
        'aadhar_number' => 'required',
        'account_num' => 'required',
        'onboarding_status' => 'required'*/
    ];

        public function user ()
        {
            return $this->belongsTo(User::class, 'id');
        }

        public function customer(){
            return $this->hasMany(Customer::class,'user_id');
        }
    
}
