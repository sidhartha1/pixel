<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class order
 * @package App\Models
 * @version November 10, 2017, 2:40 pm UTC
 *
 * @property string item
 * @property string ASIN
 * @property integer Quantity
 * @property double Price
 * @property string Amazon_Id
 * @property date date
 */
class order extends Model
{
    use SoftDeletes;

    public $table = 'orders';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'item',
        'ASIN',
        'Quantity',
        'Price',
        'Amazon_Id',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'item' => 'string',
        'ASIN' => 'string',
        'Quantity' => 'integer',
        'Price' => 'double',
        'Amazon_Id' => 'string',
        'date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'item' => 'required',
//        'ASIN' => 'required',
//        'Quantity' => 'required',
//        'Price' => 'required',
//        'Amazon_Id' => 'required',
//        'date' => 'required'
    ];

    
}
