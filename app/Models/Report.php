<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 * @package App\Models
 * @version September 7, 2017, 5:27 am UTC
 *
 * @method static Report find($id=null, $columns = array())
 * @method static Report|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string amazon_store_id
 * @property string ad_fees
 * @property string nwecustomerbonus
 * @property string excel_sheet
 */
class Report extends Model
{
    use SoftDeletes;

    public $table = 'reports';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'amazon_store_id',
        'ad_fees',
        'nwecustomerbonus',
        //'excel_sheet'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'amazon_store_id' => 'string',
        'ad_fees' => 'string',
        'nwecustomerbonus' => 'string',
       // 'excel_sheet' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
       // 'excel_sheet' => 'required'
    ];

    
}
