<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Onboarding
 * @package App\Models
 * @version August 19, 2017, 10:46 am UTC
 *
 * @method static Onboarding find($id=null, $columns = array())
 * @method static Onboarding|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string name
 * @property string url
 * @property string question
 * @property string option_1
 * @property string option_2
 * @property string option_3
 * @property string oprtion_4
 * @property string correct_ans
 */
class Onboarding extends Model
{
    use SoftDeletes;

    public $table = 'onboardings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'url',
        'question',
        'option_1',
        'option_2',
        'option_3',
        'option_4',
        'correct_ans'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'url' => 'string',
        'question' => 'string',
        'option_1' => 'string',
        'option_2' => 'string',
        'option_3' => 'string',
        'option_4' => 'string',
        'correct_ans' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'url' => 'required',
        'question' => 'required',
        'option_1' => 'required',
        'option_2' => 'required',
        'option_3' => 'required',
        'option_4' => 'required',
        'correct_ans' => 'required'
    ];

    
}
