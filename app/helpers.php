<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/17/2017
 * Time: 10:46 PM
 */

function mode ( $type = null )
{
    return ends_with ( Route::currentRouteAction (), $type );
}


function user ()
{
    if ( Auth::check () ) {
        return Auth::user ();
    }

    return null;
}

function fileUpload ( $inputFieldName, $dir = null )
{
    if ( ( Input::hasFile ( $inputFieldName ) ) ) {
        $file = Input::file ( $inputFieldName );
        if ( ! is_null ( $dir ) ) {
            $destinationPath = public_path () . '/uploads/' . $dir . '/'; // path to save to, has to exist and be writeable
            $filename = $dir . '/' . uniqid ( 'lm' ) . $file->getClientOriginalName (); // original name that it was uploaded with
        } else {
            $destinationPath = public_path () . '/uploads/';
            $filename = uniqid ( 'lm' ) . $file->getClientOriginalName (); // original name that it was uploaded with
        }

        $file->move ( $destinationPath, $filename ); // moving the file to specified dir with the original name
        return $filename;
    }
    return null;
}