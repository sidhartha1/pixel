<?php

namespace App\Repositories;

use App\Models\Onboarding;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OnboardingRepository
 * @package App\Repositories
 * @version August 19, 2017, 10:46 am UTC
 *
 * @method Onboarding findWithoutFail($id, $columns = ['*'])
 * @method Onboarding find($id, $columns = ['*'])
 * @method Onboarding first($columns = ['*'])
*/
class OnboardingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'url',
        'question',
        'option_1',
        'option_2',
        'option_3',
        'oprtion_4',
        'correct_ans'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Onboarding::class;
    }
}
