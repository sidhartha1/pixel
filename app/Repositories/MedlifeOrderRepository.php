<?php

namespace App\Repositories;

use App\MedlifeOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MedlifeOrderRepository
 * @package App\Repositories
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method MedlifeOrder findWithoutFail($id, $columns = ['*'])
 * @method MedlifeOrder find($id, $columns = ['*'])
 * @method WMedlifeOrderydr first($columns = ['*'])
 */
class MedlifeOrderRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
			'email',
			'rx_id',
			'order_id',
			'order_state',
			'customer_id',
			'customer_name',
			'upload_count',
			'delivery_name',
			'delivery_mobile',
			'delivery_add_1',
			'delivery_add_2',
			'delivery_city',
			'delivery_pincode',
			'estimated_delivery_date',
			'total_amount',
			'payable_amount',
			'discount_amount',
			'date'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
	    return MedlifeOrder::class;
	}
}
