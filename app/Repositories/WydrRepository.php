<?php

namespace App\Repositories;

use App\Wydr;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WydrRepository
 * @package App\Repositories
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method Wydr findWithoutFail($id, $columns = ['*'])
 * @method Wydr find($id, $columns = ['*'])
 * @method Wydr first($columns = ['*'])
 */
class WydrRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
			'customer_id',
			'customer_name',
			'product_name',
			'order_id',
			'quantity',
			'price',
			'paid',
			'shipping_cost',
			'balance',
			'email',
			'date'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Wydr::class;
	}
}
