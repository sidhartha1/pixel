<?php

namespace App\Repositories;

use App\ZestMoney;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ZestMoneyRepository
 * @package App\Repositories
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method ZestMoney findWithoutFail($id, $columns = ['*'])
 * @method ZestMoney find($id, $columns = ['*'])
 * @method ZestMoney first($columns = ['*'])
 */
class ZestMoneyRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
			'customer_id',
			'customer_name',
			'product_name',
			'order_id',
			'quantity',
			'price',
			'email',
			'date'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return ZestMoney::class;
	}
}
