<?php

namespace App\Repositories;

use App\Models\UserResponse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserResponseRepository
 * @package App\Repositories
 * @version August 24, 2017, 10:50 am UTC
 *
 * @method UserResponse findWithoutFail($id, $columns = ['*'])
 * @method UserResponse find($id, $columns = ['*'])
 * @method UserResponse first($columns = ['*'])
*/
class UserResponseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'owner_id',
        'question_id',
        'user_answer'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserResponse::class;
    }
}
