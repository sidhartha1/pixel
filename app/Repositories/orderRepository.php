<?php

namespace App\Repositories;

use App\Models\order;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class orderRepository
 * @package App\Repositories
 * @version November 10, 2017, 2:40 pm UTC
 *
 * @method order findWithoutFail($id, $columns = ['*'])
 * @method order find($id, $columns = ['*'])
 * @method order first($columns = ['*'])
*/
class orderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item',
        'ASIN',
        'Quantity',
        'Price',
        'Amazon_Id',
        'date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return order::class;
    }
}
