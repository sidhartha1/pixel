<?php

namespace App\Repositories;

use App\Loan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LoanRepository
 * @package App\Repositories
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method Loan findWithoutFail($id, $columns = ['*'])
 * @method Loan find($id, $columns = ['*'])
 * @method Loan first($columns = ['*'])
 */
class LoanRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
			'customer_name',
			'customer_mobile',
			'customer_email',
			'customer_age',
			'gross_monthly_salary',
			'existing_emis',
			'property_location',
			'working_location',
			'type_of_loan',
			'company_name',
			'store_email',
			'date'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Loan::class;
	}
}
