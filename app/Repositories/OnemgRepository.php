<?php

namespace App\Repositories;

use App\Onemg;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OnemgRepository
 * @package App\Repositories
 * @version August 15, 2017, 8:14 am UTC
 *
 * @method Onemg findWithoutFail($id, $columns = ['*'])
 * @method Onemg find($id, $columns = ['*'])
 * @method Onemg first($columns = ['*'])
 */
class OnemgRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
			'customer_id',
			'customer_name',
			'order_value',
			'order_id',
			'paid',
			'balance',
			'email',
			'date'
	];

	/**
	 * Configure the Model
	 **/
	public function model()
	{
		return Onemg::class;
	}
}
