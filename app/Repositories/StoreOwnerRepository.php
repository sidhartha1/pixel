<?php

namespace App\Repositories;

use App\Models\StoreOwner;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StoreOwnerRepository
 * @package App\Repositories
 * @version August 15, 2017, 5:32 am UTC
 *
 * @method StoreOwner findWithoutFail($id, $columns = ['*'])
 * @method StoreOwner find($id, $columns = ['*'])
 * @method StoreOwner first($columns = ['*'])
*/
class StoreOwnerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'dob',
        'gender',
        'phone',
        'source',
        'amazon_store_id',
        'Previous_profession',
        'pan_number',
        'aadhar_number',
        'account_num',
        'onboarding_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreOwner::class;
    }
}
