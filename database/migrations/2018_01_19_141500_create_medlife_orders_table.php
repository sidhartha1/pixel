<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedlifeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medlife_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('rx_id');
            $table->string('order_id')->nullable()->default(null);;
            $table->string('order_state')->nullable()->default(null);;
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('upload_count');
            $table->string('delivery_name');
            $table->string('delivery_mobile');
            $table->string('delivery_add_1');
            $table->string('delivery_add_2');
            $table->string('delivery_city');
            $table->string('delivery_pincode');
            $table->string('estimated_delivery_date')->nullable()->default(null);;
            $table->string('total_amount')->nullable()->default(null);;
            $table->string('payable_amount')->nullable()->default(null);;
            $table->string('discount_amount')->nullable()->default(null);;
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medlife_orders');
    }
}
