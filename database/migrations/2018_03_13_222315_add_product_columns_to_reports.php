<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductColumnsToReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->string('items_shipped')->after('amazon_store_id')->nullable()->default(null);
            $table->string('product_name')->after('amazon_store_id')->nullable()->default(null);
            $table->string('category')->after('amazon_store_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropColumn('items_shipped');
            $table->dropColumn('product_name');
            $table->dropColumn('category');
        });
    }
}
