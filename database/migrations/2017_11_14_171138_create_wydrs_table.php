<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWydrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wydrs', function (Blueprint $table) {
          $table->increments('id');
          $table->string('product_name');
          $table->integer('order_id');
          $table->integer('quantity');
          $table->integer('price');
          $table->integer('total_cost');
          $table->integer('shipping_cost');
          $table->date('date');
          $table->integer('paid');
          $table->integer('balance');
          $table->string('email');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wydrs');
    }
}
