<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amazon_store_id');
            $table->unsignedInteger('customer_id');
            $table->string('product_name');
            $table->string('product_id');
            $table->string('product_quantity');
            $table->double('price');
            $table->double('paid')->nullable()->default(null);
            $table->double('balance');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
