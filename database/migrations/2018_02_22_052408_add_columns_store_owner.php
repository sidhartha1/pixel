<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStoreOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_owners', function (Blueprint $table) {
            $table->string('town')->after('phone')->nullable()->default(null);
            $table->string('pincode')->after('phone')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_owners', function (Blueprint $table) {
            $table->dropColumn('town');
            $table->dropColumn('pincode');
        });
    }
}
