<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnemgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onemgs', function (Blueprint $table) {
          $table->increments('id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('order_id');
            $table->integer('order_value');
            $table->integer('paid');
            $table->integer('balance');
            $table->string('email');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onemgs');
    }
}
