<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreOwnersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_owners', function (Blueprint $table) {
            $table->unsignedInteger( 'id' )->unique();
            $table->string('email');
            $table->date('dob')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->bigInteger('phone')->nullable()->default(null);
            $table->string('source')->nullable()->default(null);
            $table->string('amazon_store_id')->nullable()->default(null);
            $table->string('Previous_profession')->nullable()->default(null);
            $table->string('pan_number')->nullable()->default(null);
            $table->string('aadhar_number')->nullable()->default(null);
            $table->string('account_num')->nullable()->default(null);
            $table->tinyInteger('onboarding_status')->default(0);
            $table->string('url')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_owners');
    }
}
