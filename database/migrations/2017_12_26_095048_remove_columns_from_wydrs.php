<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromWydrs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wydrs', function (Blueprint $table) {
            if(Schema::hasColumn('wydrs', 'product_category')) {
                $table->dropColumn(['product_category']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wydrs', function (Blueprint $table) {
            //
        });
    }
}
