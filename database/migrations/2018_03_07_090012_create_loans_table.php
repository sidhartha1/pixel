<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid')->nullable()->default(null);
            $table->string('customer_name');
            $table->string('customer_mobile');
            $table->string('customer_email')->nullable()->default(null);
            $table->string('customer_age')->nullable()->default(null);
            $table->string('customer_gender')->nullable()->default(null);
            $table->string('gross_monthly_salary');
            $table->string('exisiting_emis');
            $table->string('property_location')->nullable()->default(null);
            $table->string('working_location')->nullable()->default(null);
            $table->string('type_of_loan');
            $table->string('company_name')->nullable()->default(null);
            $table->integer('eligibility');
            $table->date('date');
            $table->string('store_email');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
