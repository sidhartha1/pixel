<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZestmoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zest_moneys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->string('product_name');
            $table->string('product_category');
            $table->integer('order_id');
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('total_cost');
            $table->date('date');
            $table->integer('paid');
            $table->integer('balance');
            $table->string('email');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zestmoneys');
    }
}
