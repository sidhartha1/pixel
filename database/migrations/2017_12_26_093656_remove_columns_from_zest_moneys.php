<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromZestMoneys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zest_moneys', function (Blueprint $table) {
            if(Schema::hasColumn('zest_moneys', 'product_category')) { 
                $table->dropColumn(['product_category']);
            }
            $table->dropColumn(['paid', 'balance']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zest_moneys', function (Blueprint $table) {
            //
        });
    }
}
