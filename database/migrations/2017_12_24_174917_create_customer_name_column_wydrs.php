<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerNameColumnWydrs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wydrs', function (Blueprint $table) {
            $table->string('customer_id')->after('id');
            $table->string('customer_name')->after('customer_id');
            $table->string('product_category')->after('product_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wydrs', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('customer_name');
            $table->dropColumn('product_category');
        });
    }
}
