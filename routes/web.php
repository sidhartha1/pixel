<?php

/*
 |--------------------------------------------------------------------------
 | Web Routes
 |--------------------------------------------------------------------------
 |
 | Here is where you can register web routes for your application. These
 | routes are loaded by the RouteServiceProvider within a group which
 | contains the "web" middleware group. Now create something great!
 |
 */

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/support', function () {
    return view('support');
})->name('support');

Route::get('/ibpay', function () {
    return view('ibpay.index');
})->name('ibpay');

Route::get('/zestview', function () {
    return view('zestview.index');
})->name('zestview');

Route::get('/insurance', function () {
    return view('insurance.index');
})->name('insurance');

Route::get('/knowledge', function () {
    return view('knowledge.index');
})->name('knowledge');

Route::get('/storeSales', function () {
    return view('storeSales.index');
})->name('storeSales');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('check','HomeController@check')->name('check');
Route::post('storeOwners/checkCustomer','StoreOwnerController@checkCustomer')->name('checkCustomer');
Route::post('storeOwners/submitTicket','StoreOwnerController@submitTicket')->name('submitTicket');

Route::post('storeOnboarding/processtoken', 'StoreOnboardingController@processToken');
Route::resource('storeOnboarding', 'StoreOnboardingController');

Route::post('loanyantra/checkCustomerLoans','LoanyantraController@checkCustomerLoans')->name('checkCustomerLoans');
Route::post('loanyantra/storeCustomer','LoanyantraController@storeCustomer')->name('storeCustomer');
Route::resource('loanyantra', 'LoanyantraController');

Route::post('nosales', 'CustomerController@nosales')->name('nosales');
Route::resource('storeOwners', 'StoreOwnerController');
Route::resource('customers', 'CustomerController');

Route::get('sale/{filter}','SaleController@filter')->name('salefilter');

Route::resource('sales', 'SaleController');
Route::resource('onboardings', 'OnboardingController');
Route::resource('userResponses', 'UserResponseController');

Route::get('download-excel-file/{type}','ReportController@downloadExcelFile')->name('excel-file');
Route::get('download-latest/{type}','ReportController@lastRecords')->name('excel');
Route::get('report/{filter}','ReportController@filter')->name('filter');

Route::resource('reports', 'ReportController');
Route::resource('orders', 'orderController');
Route::resource('wydr','WydrController');
Route::resource('medlife','MedlifeController');
Route::resource('zestmoney','ZestMoneyController');
Route::resource('onemg','OnemgController');

Route::get('customersales/{filter}', 'CustomerController@salesfilter')->name('customersales');

Route::get('order/{filter}','orderController@filter')->name('orderfilter');
Route::get('wydrsales/{filter}','WydrController@filter')->name('wydrfilter');
Route::get('medlifesales/{filter}','MedlifeController@filter')->name('medlifefilter');
Route::get('zestmoneysales/{filter}','ZestMoneyController@filter')->name('zestmoneyfilter');
Route::get('onemgsales/{filter}','OnemgController@filter')->name('onemgfilter');
Route::post('medlife/updateorder', 'MedlifeController@updateOrder');

Route::get('/migrations', function () {
    try {
        dump('Init with app tables migrations...');
        dump(Artisan::call( 'migrate', ['--step'=> ''] ));
        dump('Done with app tables migrations');
    } catch (Exception $e) {
        print_r($e->getMessage());
        Response::make($e->getMessage(), 500);
    }
});

Route::get('/validation', 'LoanyantraController@getLoginCredentials');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
